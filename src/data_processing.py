import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import os

from mpl_toolkits.mplot3d.axes3d import get_test_data
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import pypcd

import sys
import numpy as np
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("prob_grasp_planner")  + "/src/grasp_common_library")
from data_proc_lib import DataProcLib
from voxel_visualization_tools import visualize_voxel, convert_to_dense_voxel_grid

'''
This script voxelizes pcd files.
'''

root_dir= "/mnt/data_collection/all_data/"
pcd_files= [root_dir + f for f in os.listdir(root_dir) if f.endswith(".pcd")]

data_proc_lib = DataProcLib()

for i, pcd_file in enumerate(pcd_files):
    sparse_voxel_grid = data_proc_lib.voxel_gen_client_file(pcd_file)
    dense_voxel_grid = convert_to_dense_voxel_grid(sparse_voxel_grid, 32)
    #visualize_voxel(dense_voxel_grid, as_cubes=True)
    no_extension_path = os.path.splitext(pcd_file)[0]
    with open(no_extension_path + ".npy", "wb") as f:
        np.save(f, dense_voxel_grid)
    if i % 100 == 0:
        print(i * 100.0 / len(pcd_files))

