import trimesh
import numpy as np
import rospy
from grasp_pipeline.srv import SDFQuery, SDFQueryResponse

INCHES_TO_METERS_CONVERSION = 0.0254

class ObjectRepresentation():
    '''
    This class abstracts object representation, which can be either SDF or true mesh.
    All the points queried through this API are assumed to be in object frame.
    '''
    def __init__(self, sdf_and_gradient_fn, mesh_path):
        assert sdf_and_gradient_fn is None or mesh_path is None
        self.sdf_and_gradient_fn = sdf_and_gradient_fn
        self.mesh_path = mesh_path
        self.pose = None
        self.size = None
        self.mesh = None
        if self.mesh_path is not None:
            self.mesh = trimesh.load(self.mesh_path)
            print("unit guess: ")
            trimesh.units.units_from_metadata(self.mesh)

        # used for SDF
        self.processed_cloud = None
        self.cloud_scale = None


        # for compatibility with Python 3 TODO not necessary anymore
        #rospy.init_node("object_representation_node")
        sdf_service = rospy.Service("get_sdf", SDFQuery, self.sdf_service)

    def get_bounding_box(self, epsilon=0.015):
        return [self.size[0] + epsilon, self.size[1] + epsilon, self.size[2] + epsilon]

    def sdf_service(self,req):
        point = np.array([req.point])
        sdf, gradient = self.get_sdf_and_gradient(point)
        resp = SDFQueryResponse()
        resp.gradient = gradient.tolist()
        resp.sdf = sdf
        return resp

    def update_cloud_representation(self, processed_cloud, scale):
        self.processed_cloud = processed_cloud
        self.scale = scale
        self.mesh = None
        self.mesh_path = None

    def update_mesh_representation(self, mesh_path):
        self.mesh_path = mesh_path
        self.mesh = trimesh.load(mesh_path)
        self.processed_cloud = None
        self.cloud_scale = None

    def get_closest_point_on_surface(self, point):
        assert point.shape == (1,3), point.shape
        if self.mesh is not None:
            return self._get_closest_point_on_surface_from_mesh(point)
        else:
            return self._get_closest_point_on_surface_from_dnn(point)

    def get_closest_points_on_surface(self, points):
        '''
        Expects 4 points as numpy array(4,3).
        Returns closest points(4x3) on surface and sdf (4,) to them from queried points.
        '''
        assert points.shape == (4,3), points.shape
        closest_points = np.zeros(points.shape)
        sdfs = np.zeros(points.shape[0])
        for i in range(points.shape[0]):
            point = np.expand_dims(points[i,:],axis=0)
            closest_point, sdf = self.get_closest_point_on_surface(point)
            closest_points[i,:] = closest_point
            sdfs[i] = sdf
        return closest_points, sdfs

    def get_sdf(self, point):
        if self.mesh:
            sdf, _ = self._get_distance_and_gradient_from_mesh(point)
        else:
            sdf, _ = self._get_sdf_and_gradient_from_dnn(point)
        return sdf

    def get_gradient(self, point):
        if self.mesh:
            _, gradient = self._get_distance_and_gradient_from_mesh(point)
        else:
            _, gradient = self._get_sdf_and_gradient_from_dnn(point)
        return gradient

    def get_sdf_and_gradient(self, point):
        '''
        Input point is in object frame and is not scaled in any way shape or form.
        '''
        assert point.shape == (1,3), point.shape
        if self.mesh:
            return self._get_sdf_and_gradient_from_mesh(point)
        else:
            return self._get_sdf_and_gradient_from_dnn(point)

    def _get_sdf_and_gradient_from_dnn(self, point):
        point_scaled = point * self.scale
        sdf, gradient = self.sdf_and_gradient_fn(self.processed_cloud, point_scaled)
        sdf /= self.scale
        gradient /= self.scale
        return sdf, gradient

    def _get_sdf_and_gradient_from_mesh(self, point):
        closest_point, distance = self._get_closest_point_on_surface_from_mesh(point)
        gradient = closest_point - point
        return distance, gradient

    def _get_closest_point_on_surface_from_mesh(self, point):
        closest_point, distance, _ = self.mesh.nearest.on_surface(point)
        return closest_point, distance

    def _get_closest_point_on_surface_from_dnn(self, point):
        sdf, gradient = self.get_sdf_and_gradient(point)
        gradient = np.squeeze(np.array(gradient))
        for i in range(len(gradient)):
            gradient[i] += 1e-6
        gradient /= np.linalg.norm(gradient)
        gradient *= sdf
        return point - gradient, sdf

