#!/usr/bin/env python
import os
import rospy
import time
import trimesh
import tf
import numpy as np
import roslib
import sys
from robot_movements import move_robot_home, move_arm_up, move_arm
from grasp_interface import GraspClientInterface
from object_representation import ObjectRepresentation
from geometry_msgs.msg import Pose, PoseStamped
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import cv2
from datetime import datetime
import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("grasp_pipeline"))
from grasp_pipeline.srv import *

def main():

    grasp_interface = GraspClientInterface(simulation = False, reconstruct_mesh=False, marching_cubes=True)
    while True:
        #move_robot_home(grasp_interface.moveit_planner,
        #        grasp_interface.hand_client, grasp_interface.scene_manager)

        seg_obj, object_id, trajectory, hand_q = grasp_interface.move_to_preshape()

        #input("press enter to move hand to preshape config")
        #grasp_interface.hand_client.send_pos_cmd(hand_q, contact_sensing=False)
        #input("press enter to move arm to preshape config")
        #move_arm(trajectory=trajectory)

        # evaluate preshape
        print("line 39")
        position, orientation = grasp_interface.get_current_palm_pose_in_object_frame()
        
        print("line 42")
        palm_pose = Pose()
        palm_pose.orientation.x,\
        palm_pose.orientation.y,\
        palm_pose.orientation.z,\
        palm_pose.orientation.w = orientation
 
        palm_pose.position.x,\
        palm_pose.position.y,\
        palm_pose.position.z = position

        palm_pose_msg = PoseStamped()
        palm_pose_msg.pose = palm_pose
        palm_pose_msg.header.frame_id = "estimated_object_pose"
        hand_config = grasp_interface.hand_client.get_joint_state()
        
        service_name = "evaluate_grasp"
        proxy = rospy.ServiceProxy(service_name, GraspInfo)
        request = GraspInfoRequest()
        object_info = seg_obj
        request.obj = object_info
        request.palm_pose_obj_frame = palm_pose_msg
        request.full_allegro_joint_state = hand_config
        rospy.wait_for_service(service_name)
        grasp_response = proxy(request)
        rospy.loginfo("[experiment info] Preshape evaluation: ")
        rospy.loginfo(grasp_response)


        input("press enter to close hand")
        grasp_interface.close_hand(gauss_newton=True)
        print("hand closed")
        #grasp_interface.close_hand(heuristic=True)

        # evaluate grasp
        hand_config_in_contact = grasp_interface.hand_client.get_joint_state()
        service_name = "evaluate_in_hand"
        proxy = rospy.ServiceProxy(service_name, GraspInfo)
        request.full_allegro_joint_state = hand_config_in_contact
        rospy.loginfo("waiting for service..")
        rospy.wait_for_service(service_name)
        rospy.loginfo("Evaluating grasp..")
        grasp_response = proxy(request)
        line = "Grasp evaluated: "
        rospy.loginfo("[in hand log likelihood]" + str(grasp_response.in_hand_success_log_likelihood))
        rospy.loginfo("in percentage: " + str(np.exp(grasp_response.in_hand_success_log_likelihood)))

        ftips_in_contact = grasp_interface.hand_client.get_biotac_contact_readings()
        print("ftips in contact before squeezing: ", sum(ftips_in_contact))


        #ftips_in_contact = grasp_interface.stabilize_grasp(gauss_newton=True)
        #ftips_in_contact = grasp_interface.stabilize_grasp(heuristic=True)
        print("ftips in contact after squeezing: ", sum(ftips_in_contact))

        move_arm_up(grasp_interface.moveit_planner, grasp_interface.scene_manager)
        input("done!")

    #move_robot_home(grasp_interface.moveit_planner,grasp_interface.hand_client)
    #grasp_interface.hand_closing_controller.increase_stiffness_via_J()
    #grasp_interface.scene_manager.update_simulation_scene()
    #grasp_interface.prepare_object()
    '''
    grasp_interface.hand_closing_controller.update_object_representation(grasp_interface.object_representation)
    grasp_interface.hand_closing_controller.follow_sdf_gradient()
    grasp_interface.hand_closing_controller.follow_sdf_gradient()
    grasp_interface.hand_closing_controller.follow_sdf_gradient()
    grasp_interface.hand_closing_controller.follow_sdf_gradient()
    grasp_interface.hand_closing_controller.follow_sdf_gradient()
    '''
    '''
    i = 0
    while True:
        move_robot_home(grasp_interface.moveit_planner,grasp_interface.hand_client)
        if i % 2 == 0:
            grasp_interface.scene_manager.update_simulation_scene()
            seg_obj, name, traj, hand_q = grasp_interface.move_to_preshape()
        if i % 2 == 1:
            grasp_interface.scene_manager.repeat_simulation_scene()
            grasp_interface.hand_client.send_pos_cmd(hand_q)
            move_arm(trajectory=trajectory)

        i+=1
      
        object_representation = ObjectRepresentation(grasp_interface.reconstructor.get_sdf_and_gradient, None)
        processed_cloud, length, scale, centroid_diff = grasp_interface.reconstructor.get_processed_pointcloud_from_msg(seg_obj.cloud)
        object_representation.update_cloud_representation(processed_cloud, scale)

        pts = np.array([[np.random.rand()*.2,np.random.rand()*.2,np.random.rand()*.1],
                        [np.random.rand()*.2,np.random.rand()*.2,np.random.rand()*.1], 
                        [np.random.rand()*.2,np.random.rand()*.2,np.random.rand()*.1], 
                        [np.random.rand()*.2,np.random.rand()*.2,np.random.rand()*.1]])
        frames = ["estimated_object_pose","estimated_object_pose","estimated_object_pose","estimated_object_pose"]
        grasp_interface.visualization.update_points_markers(pts, frames)

        for i in range(500):
            pts, distances = object_representation.get_closest_points_on_surface(pts)
            raw_input('press enter to update vis')
            print('distances: ', distances)
            grasp_interface.visualization.update_points_markers(pts, frames)
        rospy.loginfo("Moved to preshape!")
        position, orientation = grasp_interface.get_current_palm_pose_in_object_frame()
        
        palm_pose = Pose()
        palm_pose.orientation.x,\
        palm_pose.orientation.y,\
        palm_pose.orientation.z,\
        palm_pose.orientation.w = orientation
 
        palm_pose.position.x,\
        palm_pose.position.y,\
        palm_pose.position.z = position

        palm_pose_msg = PoseStamped()
        palm_pose_msg.pose = palm_pose
        palm_pose_msg.header.frame_id = "estimated_object_pose"
        hand_config = grasp_interface.hand_client.get_joint_state()
        
        service_name = "evaluate_grasp"
        proxy = rospy.ServiceProxy(service_name, GraspInfo)
        request = GraspInfoRequest()
        object_info = seg_obj
        request.obj = object_info
        request.palm_pose_obj_frame = palm_pose_msg
        request.full_allegro_joint_state = hand_config
        rospy.wait_for_service(service_name)
        grasp_response = proxy(request)
        rospy.loginfo("[experiment info] Preshape evaluation: ")
        rospy.loginfo(grasp_response)

        rospy.wait_for_service("update_robot_state")
        proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
        vis_req = UpdateRobotStateRequest()
        vis_req.joint_state = []
        proxy(vis_req)

        if i%2 == 0:
            rospy.loginfo("using GN to close hand")
            grasp_interface.close_hand(gauss_newton=True)
        else:
            grasp_interface.close_hand(heuristic=True)

        raw_input("press enter to move arm up")
        move_arm_up(grasp_interface.moveit_planner, grasp_interface.scene_manager)
    '''
if __name__ == '__main__':
    main()
