import rospy
import sys
import roslib.packages as rp
import numpy as np
sys.path.append(rp.get_pkg_dir('tf_tools') + '/scripts')
from tf_client import TFClient
from geometry_msgs.msg import PointStamped
from hand_closing_utils import forward_kinematics_kdl, fk_for_finger
from line_search import get_current_state
from plane_utils import construct_plane, project_on_plane

FLOAT_PRECISION = 1e-6

class HandAdaptationController:
    def __init__(self, hand_kdl, hand_client):
        self.hand_client = hand_client
        self.hand_kdl = hand_kdl

        # latest point is the rightmost column
        self.index_points = np.random.rand(3,3) * 0.01 * 0
        self.middle_points = np.random.rand(3,3) * 0.01 * 0
        self.ring_points = np.random.rand(3,3) * 0.01 * 0
        self.thumb_points = np.random.rand(3,3) * 0.01 * 0
        self.points_frames = ['index_biotac_origin','middle_biotac_origin','ring_biotac_origin','thumb_biotac_origin']

        rospy.Subscriber("biotac/index_tip/contact_point", PointStamped, self.index_contact_callback)
        rospy.Subscriber("biotac/middle_tip/contact_point", PointStamped, self.middle_contact_callback)
        rospy.Subscriber("biotac/ring_tip/contact_point", PointStamped, self.ring_contact_callback)
        rospy.Subscriber("biotac/thumb_tip/contact_point", PointStamped, self.thumb_contact_callback)
        self.vis_client = TFClient()
        self.planes = None

    def get_next_target_points(self, q_current, q_gradient):
        q_index_grad, q_middle_grad, q_ring_grad, q_thumb_grad = q_gradient
        target_points, _ = forward_kinematics_kdl(q_index_grad, q_middle_grad,
                                                  q_ring_grad, q_thumb_grad, self.hand_kdl)
        target_points = [np.expand_dims(target_points[:, i], 1) for i in range(target_points.shape[1])]
        return self.project_on_object_manifold(q_current, target_points)

    def project_on_object_manifold(self, q_all, points):
        assert points[0].shape == (3,1), points[0].shape
        projected_points = []
        planes = self.update_planes()
        for i in range(4):
            q_finger = q_all[i]
            target_point = points[i] # PF
            axis, origin = self.planes[i]
            normal = axis[2]

            raw_input("press enter to visualize point in palm_link frame")
            self.vis_client.update_projected_point(target_point, "palm_link")
            target_point = self.convert_pf_to_biotac_origin_frame(target_point, q_finger, i) # BO frame

            raw_input("press enter to visualize point in finger frame")
            self.vis_client.update_projected_point(target_point, self.points_frames[i])

            target_point_projected = project_on_plane(target_point, normal, origin) # projection is in biotac origin frame

            raw_input("press enter to visualize projected point in finger frame")
            self.vis_client.update_projected_point(target_point_projected, self.points_frames[i])

            target_point_projected = self.convert_bo_to_pf(target_point_projected, q_finger, i) # Palm Frame

            raw_input("press enter to visualize projected point in palm_link frame")
            self.vis_client.update_projected_point(target_point_projected, "palm_link")

            projected_points.append(target_point_projected)
        return projected_points

    def convert_pf_to_biotac_origin_frame(self, point_pf, q, finger_id):
        
        T_bo_to_pf = fk_for_finger(finger_id, q, self.hand_kdl, as_homogeneous = True, end_link = "biotac_origin")

        # we need inverse of that
        rot_inv = np.eye(4)
        rot_inv[:3,:3] = T_bo_to_pf[:3,:3].T
        transl_inv = np.eye(4)
        transl_inv[0,3] = - T_bo_to_pf[0,3]
        transl_inv[1,3] = - T_bo_to_pf[1,3]
        transl_inv[2,3] = - T_bo_to_pf[2,3]
        T_pf_to_bo = np.dot(rot_inv, transl_inv)

        point_pf = np.append(point_pf, np.ones((1,1)), 0) # add 1 to axis 0

        point_bo_hom = np.dot(T_pf_to_bo, point_pf)
        point_bo = point_bo_hom[:3,0]
        point_bo = np.squeeze(np.asarray(point_bo))

        return point_bo

    def convert_bo_to_pf(self, point_bo, q, finger_id):
        T_bo_to_pf = fk_for_finger(finger_id, q, self.hand_kdl, as_homogeneous = True, end_link = "biotac_origin")
        point_bo_hom = np.append(point_bo, np.ones((1,1)), 0) # add 1 to axis 0
        point_pf_hom = np.dot(T_bo_to_pf, point_bo_hom)
        point_pf = np.squeeze(np.asarray(point_pf_hom[:3,0]))
        return point_pf

    def update_planes(self):
        self.planes = self.get_planes()
        self.vis_client.update_planes(self.planes, self.points_frames)

    def get_planes(self):
        index = get_plane(self.index_points)
        middle = get_plane(self.middle_points)
        ring = get_plane(self.ring_points)
        thumb = get_plane(self.thumb_points)
        return (index, middle, ring, thumb)

    ### contact callbacks and updates ###

    def index_contact_callback(self, data):
        point = data.point
        if point_in_contact(point) and new_contact(self.index_points[:, -1], point):
            self.index_points[:, :-1] = self.index_points[:, 1:]
            self.index_points[0, -1] = point.x
            self.index_points[1, -1] = point.y
            self.index_points[2, -1] = point.z
            self.update_vis_client()

    def middle_contact_callback(self, data):
        point = data.point
        if point_in_contact(point) and new_contact(self.middle_points[:, -1], point):
            self.middle_points[:, :-1] = self.middle_points[:, 1:]
            self.middle_points[0, -1] = point.x
            self.middle_points[1, -1] = point.y
            self.middle_points[2, -1] = point.z
            self.update_vis_client()

    def ring_contact_callback(self, data):
        point = data.point
        if point_in_contact(point) and new_contact(self.ring_points[:, -1], point):
            self.ring_points[:, :-1] = self.ring_points[:, 1:]
            self.ring_points[0, -1] = point.x
            self.ring_points[1, -1] = point.y
            self.ring_points[2, -1] = point.z
            self.update_vis_client()

    def thumb_contact_callback(self, data):
        point = data.point
        if point_in_contact(point) and new_contact(self.thumb_points[:, -1], point):
            self.thumb_points[:, :-1] = self.thumb_points[:, 1:]
            self.thumb_points[0, -1] = point.x
            self.thumb_points[1, -1] = point.y
            self.thumb_points[2, -1] = point.z
            self.update_vis_client()

    def update_vis_client(self):
        points = [self.index_points, self.middle_points, self.ring_points, self.thumb_points]
        self.vis_client.update_points_markers(points, self.points_frames)

   
def get_plane(points):
    axis = construct_plane(points)
    origin = points[:,-1]
    return (axis, origin)

def point_in_contact(point):
    return abs(point.x) > FLOAT_PRECISION or abs(point.y) > FLOAT_PRECISION or abs(point.z) > FLOAT_PRECISION

def new_contact(old_contact_point, new_contact_point):
    return abs(old_contact_point[0] - new_contact_point.x) > FLOAT_PRECISION or\
           abs(old_contact_point[1] - new_contact_point.y) > FLOAT_PRECISION or\
           abs(old_contact_point[2] - new_contact_point.z) > FLOAT_PRECISION 
        


