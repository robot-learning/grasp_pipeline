import numpy as np

def construct_plane(points):
    '''
    Constructs a plane based on PCA algorithm. In particular, based on covariance matrix and eigenvectors.

    @param points numpy array of points where each point is represented as a column vector. Every point is 3D.
    @return 3 column vectors: 2 eigenvectors and a normal, which define the plane
    '''
    assert points.shape[0] == 3, points.shape # 3 rows because points are column vectors

    cov_matrix = np.cov(points)
    eig_values, eig_vectors = np.linalg.eig(cov_matrix)

    # get two largest eig_vectors
    idx = eig_values.argsort()[-2:][::-1]
    eig_values = eig_values[idx]
    eig_vectors = eig_vectors[:,idx]

    eig_vector_1 = eig_vectors[:,0]
    eig_vector_2 = eig_vectors[:,1]
    normal = np.cross(eig_vector_1, eig_vector_2)

    eig_vector_1 = np.expand_dims(eig_vector_1, axis = 1)
    eig_vector_2 = np.expand_dims(eig_vector_2, axis = 1)
    normal = np.expand_dims(normal, axis = 1)

    return eig_vector_1, eig_vector_2, normal

def project_on_plane(point, plane_normal, point_in_plane):
    # https://math.stackexchange.com/questions/100761/how-do-i-find-the-projection-of-a-point-onto-a-plane

    x,y,z = point[0], point[1], point[2]
    a,b,c = plane_normal[0], plane_normal[1], plane_normal[2]
    t = np.dot(plane_normal.T, point_in_plane) - np.dot(plane_normal.T, point)

    result = np.zeros((3,1))
    result[0,0] = x + t*a
    result[1,0] = y + t*b
    result[2,0] = z + t*c
    return result
