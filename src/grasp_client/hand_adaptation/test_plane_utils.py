import numpy as np
import math
from plane_utils import construct_plane

# Two points in 2D: (1,1) and (2,2)

points = np.zeros((3,2))
points[0,0] = 1
points[1,0] = 1

points[0,1] = 2
points[1,1] = 2

eig_v1, eig_v2, normal = construct_plane(points)

assert abs(eig_v1[0] - math.sqrt(0.5)) < 1e-6
assert abs(eig_v1[0] - eig_v1[1]) < 1e-6
assert abs(normal[2] - 1) < 1e-6
assert eig_v1.shape == (3,1), eig_v1.shape
assert eig_v2.shape == (3,1), eig_v2.shape
assert normal.shape == (3,1), normal.shape
