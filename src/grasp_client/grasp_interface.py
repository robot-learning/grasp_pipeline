import rospy
import tf
import mcubes
import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src") 
import align_object_frame as align_obj 
from robot_movements import move_arm, plan_trajectory
from scene_manager import SceneManager
from object_representation import ObjectRepresentation
from utils import call_service
sys.path.append(rp.get_pkg_dir("point_cloud_segmentation"))
from point_cloud_segmentation.srv import *
from grasp_pipeline.srv import *
sys.path.append(rp.get_pkg_dir("hand_services") + "/scripts")
from hand_client import HandClient
sys.path.append(rp.get_pkg_dir("point_cloud_tools"))
from point_cloud_tools.srv import *
sys.path.append(rp.get_pkg_dir("pointsdf_reconstruction") + "/src")
sys.path.append(rp.get_pkg_dir("pointsdf_reconstruction") + "/src/PointConv")
from api import MeshReconstructor
sys.path.append(rp.get_pkg_dir('tf_tools') + '/scripts')
from tf_client import TFClient
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src")
from move_it_planner import MoveItPlanner
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/grasp_client/hand_closing")
from hand_closing import HandClosingController
#sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/grasp_client/hand_adaptation")
#from hand_adaptation import HandAdaptationController
sys.path.append(rp.get_pkg_dir("prob_grasp_planner"))
from prob_grasp_planner.srv import *
import numpy as np
sys.path.append(rp.get_pkg_dir('ll4ma_kdl') + '/scripts')
from handModel import HandModel

# for voxelization start
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np

from mpl_toolkits.mplot3d.axes3d import get_test_data
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import pypcd

import sys
import numpy as np
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("prob_grasp_planner")  + "/src/grasp_common_library")
from voxel_visualization_tools import visualize_voxel, convert_to_dense_voxel_grid
from data_proc_lib import DataProcLib
from geometry_msgs.msg import Pose, Point, Quaternion
from sensor_msgs.msg import JointState
# for voxelization end 

ROSPARAM_ALLEGRO_DESCRIPTION = "allegro/robot_description"
OBJECT_FRAME = "estimated_object_pose"
ARM_NAME = "lbr4"

class GraspClientInterface:
    def __init__(self, simulation=False, reconstruct_mesh=False, marching_cubes=False, icp=False, hand_only=False):
        rospy.init_node("grasp_interface") 
        assert sum([reconstruct_mesh, marching_cubes, icp]) <= 1
        if sum([reconstruct_mesh, marching_cubes, icp]) == 0:
            rospy.loginfo("Using ground truth pose and mesh")
            self.pose_ground_truth = True
        else:
            self.pose_ground_truth = False
        self.listener = tf.TransformListener()
        #self.scene_manager = SceneManager(simulation, dataset_name="YCB_test")
        self.scene_manager = SceneManager(simulation, dataset_name="BigBird")
        self.simulation = simulation
        self.hand_client = HandClient(kinematics_only = False)
        self.hand_kdl = HandModel(ROSPARAM_ALLEGRO_DESCRIPTION)
        if marching_cubes or icp or reconstruct_mesh:
            self.hand_closing_controller = HandClosingController(self.hand_kdl, self.hand_client, self.listener, "OPTIMIZATION", OBJECT_FRAME)
        else:
            #true mesh from Gazebo
            self.hand_closing_controller = HandClosingController(self.hand_kdl, self.hand_client, self.listener, "OPTIMIZATION", "true_mesh_pose")
        #self.hand_adaptation_controller = HandAdaptationController(self.hand_kdl, self.hand_client)
        self.visualization = TFClient()
        self.reconstructor = None
        self.object_representation = None
        self.mesh_path = None
        if reconstruct_mesh is True:
            rospy.loginfo("Initializing mesh reconstructor and SDF object representation")
            self.reconstructor = MeshReconstructor()
            self.object_representation = ObjectRepresentation(self.reconstructor.get_sdf_and_gradient, None)
        else:
            rospy.loginfo("Initializing mesh-based object representation")
            self.object_representation = ObjectRepresentation(None, None)
        self.marching_cubes = marching_cubes
        self.icp = icp
        self.hand_only = hand_only
        self.data_proc_lib = DataProcLib()
        self.moveit_planner = MoveItPlanner()
        rospy.loginfo("GraspClientInterface initialized!")

    def move_to_preshape(self):
        segmented_object = self.prepare_object()

        # plan the grasp
        rospy.loginfo("planning a grasp")
        for i in range(5):
            try:
                arm_q, trajectory, hand_joints = self.plan_grasp(segmented_object, self.hand_only, self.hand_client) 
            except Exception as e:
                continue
            if trajectory is None:
                continue
            else:
                break
        # couldn't find a valid trajectory
        if trajectory is None:
            rospy.loginfo("planning grasp failed")
            return None, None, None, None

        # move robot
        #rospy.loginfo("moving the robot")
        if hand_joints is None:
            return None, None, None, None
        #self.hand_client.send_pos_cmd(hand_joints.position, False)
        # if not self.hand_only:
        #    move_arm(trajectory=trajectory)
 
        # return info for data collection
        return segmented_object, self.scene_manager.object_name, trajectory, hand_joints.position
 

    def prepare_object(self):
        # 1. segment object
        rospy.loginfo("segmenting object ...")
        response = self.segment_object()
        if not response.object_found:
            rospy.logerr("No object found from segmentation")
        segmented_object = response.obj
 
         
        # 2. obtain mesh _somehow_ for moveit scene
        rospy.loginfo("obtaining mesh ...")
        mesh_path = self.get_mesh(segmented_object)

        # 3. update moveit scene
        rospy.loginfo("updating scene ...")
        
        user_input = "R"
        while user_input == "R" and self.icp:
            segmented_object.pose = fit_mesh_via_icp(mesh_path, segmented_object.cloud)
            self.scene_manager.update_moveit_scene(mesh_path, segmented_object.pose)
            env = self.scene_manager.get_environment_description()
            self.moveit_planner.update_env_scene(env)
            user_input = input("Mesh fitted & scene updated. Please press ENTER to continue or input 'R' to repeat ICP.")

        if not self.pose_ground_truth:
            self.object_representation.mesh_path = mesh_path
            self.object_representation.pose =  segmented_object.pose
        self.object_representation.size = [segmented_object.height, segmented_object.width, segmented_object.depth]

       # visualize mesh
        if self.pose_ground_truth:
            transl, rot = self.get_true_object_pose()
            self.visualization.update_mesh_pose(transl, rot, "true_mesh_pose")
            self.visualization.update_marker_mesh("file://" + mesh_path, "true_mesh_pose")
        else:
            self.visualization.update_mesh_pose(segmented_object.pose.position, 
                                                segmented_object.pose.orientation, 
                                                OBJECT_FRAME)
            self.visualization.update_marker_mesh("file://" + mesh_path, OBJECT_FRAME)
            x = segmented_object.height + 0.015
            y = segmented_object.width + 0.015
            z = segmented_object.depth + 0.015
            self.visualization.update_box_mesh(OBJECT_FRAME, x, y, z)

        # 3a update object representation
        if self.reconstructor is not None:
            processed_cloud, _, scale = self.reconstructor.get_processed_pointcloud_from_msg(segmented_object.cloud)
            self.object_representation.update_cloud_representation(processed_cloud,scale)
        else:
            self.object_representation.update_mesh_representation(self.mesh_path)

        env = self.scene_manager.update_object_representation(self.object_representation)
        env = self.scene_manager.get_environment_description()
        self.moveit_planner.update_env_scene(env)

        return segmented_object

        
    def get_mesh(self, segmented_object):
        if self.marching_cubes:
            rospy.loginfo("marching cubes from partial view")
            sparse_voxel_grid, voxel_size, dim = self.data_proc_lib.voxel_gen_client(segmented_object)
            #visualize_voxel(sparse_voxel_grid, as_cubes=True, dense_voxel_grid=False)
            voxel_grid = convert_to_dense_voxel_grid(sparse_voxel_grid, dim[0])

            # not sure if this is needed
            voxel_grid = np.pad(voxel_grid, ((1,1),(1,1),(1,1)), mode='constant') 
            vertices, triangles = mcubes.marching_cubes(voxel_grid, 0)

            vertices = vertices * voxel_size

            # Center mesh.
            vertices[:,0] -= voxel_size * 17.0
            vertices[:,1] -= voxel_size * 17.0
            vertices[:,2] -= voxel_size * 17.0

            mesh_path = "/home/mmatak/result.obj"
            mcubes.export_obj(vertices, triangles, mesh_path)
        elif self.reconstructor is not None:
            rospy.loginfo("reconstructing mesh")
            # reconstructed mesh
            mesh_path = self.reconstructor.reconstruct_from_msg(segmented_object.cloud)
        elif self.icp:
            rospy.loginfo("Fitting a mesh via ICP")
            mesh_path = raw_input("Enter a mesh path for ICP: ")
        else:
            # true mesh
            rospy.loginfo("Using true mesh.")
            mesh_path = self.scene_manager.object_mesh_path 
        self.mesh_path = mesh_path # self.mesh_path is used later for closing the hand
        return mesh_path
  
    def close_hand(self, gauss_newton=False, heuristic=False):
        self.reset_biotac()

        if heuristic:
            self.hand_closing_controller.method = "HEURISTIC"
        else:
            self.hand_closing_controller.update_object_representation(self.object_representation)
            self.hand_closing_controller.method = "OPTIMIZATION"
        self.hand_closing_controller.close_hand(gauss_newton)

    def stabilize_grasp(self, gauss_newton=False, heuristic=False):
        go = raw_input("input y to in increase stiffness")
        if go == 'y':
            if heuristic:
                print("increasing stiffnes via heuristic approach")
                self.hand_client.increase_stiffness()
            else:
                for i in range(5):
                    self.hand_closing_controller.follow_sdf_gradient()
                    contacts = self.hand_client.get_biotac_contact_readings()
                    if sum(contacts) == 4:
                        rospy.loginfo("all fingertips in contact!")
                        break
                #self.hand_closing_controller.increase_stiffness_via_J()
                self.hand_client.increase_stiffness()

        return self.hand_client.get_biotac_contact_readings()

    def segment_object(self):
        response = call_service("object_segmenter", SegmentGraspObjectRequest(), SegmentGraspObject)
        # get dimensions correct
        response.obj = align_obj.align_object(response.obj, self.listener)
        # estimated pose
        self.visualization.update_object_pose(response.obj.pose.position,
                                                     response.obj.pose.orientation, 
                                                     OBJECT_FRAME)
        # mesh true pose
        if self.simulation and self.pose_ground_truth:
            transl, rot = self.get_true_object_pose()
            self.visualization.update_mesh_pose(transl, rot, "true_mesh_pose")
        return response

    def get_true_object_pose(self):
        return self.scene_manager.get_true_object_pose(self.listener)

    def get_estimated_object_pose(self):
        return self.get_pose_tf("world", OBJECT_FRAME)

    def reset_biotac(self):
        self.hand_client.reset_tare() # this also updates contact readings
        print("biotac tared")

    def get_current_palm_pose_in_object_frame(self):
        # used only for data collection
        return self.get_pose_tf(OBJECT_FRAME, "palm_link")

    def get_ftips_poses(self, frame):
        ftips = ["index_biotac_surface", "middle_biotac_surface", "ring_biotac_surface", "thumb_biotac_surface"]
        positions = []
        rotations = []
        for ftip in ftips:
            position, rotation = self.get_pose_tf(frame, ftip)
            positions.extend(position)
            rotations.extend(rotation)
        return positions, rotations

    def get_pose_tf(self, source, target, as_ros_pose = False):
        self.listener.waitForTransform(source, target, rospy.Time(), rospy.Duration(4.0))
        pose = self.listener.lookupTransform(source, target, rospy.Time(0))
        if as_ros_pose:
            position, orientation = pose
            position_ROS = Point()
            position_ROS.x = position[0]
            position_ROS.y = position[1]
            position_ROS.z = position[2]
            orientation_ROS = Quaternion()
            orientation_ROS.x = orientation[0]
            orientation_ROS.y = orientation[1]
            orientation_ROS.z = orientation[2]
            orientation_ROS.w = orientation[3]
            pose_ROS = Pose()
            pose_ROS.position = position_ROS
            pose_ROS.orientation = orientation_ROS 
            return pose_ROS
        return pose

    def plan_grasp(self, obj, hand_only, hand_client):
        request = GraspPreshapeRequest()
        request.obj = obj

        # used by collision checker..
        if self.pose_ground_truth:
            request.use_true_mesh = True
            request.true_mesh_pose = self.get_pose_tf("world", "true_mesh_pose", True)
        
        arm_q = None
        #trajectory, hand_joints = self.heuristic_grasp(request, hand_only, hand_client)
        while arm_q is None:
            arm_q, trajectory, hand_joints = self.tactile_grasp(request, hand_only, hand_client)
        #arm_q, trajectory, hand_joints = active_learning_grasp(request)
     
        return arm_q, trajectory, hand_joints

    def tactile_grasp(self, request, hand_only, hand_client):
        number_of_grasp_candidates = 1
        service_name = "grasp_tactile_plan"
        grasp_response = call_service(service_name, request, GraspPreshape)
        if grasp_response.success == False:
            print("Planning grasp failed")
            return None, None, None

        allegro_joints_position = grasp_response.allegro_joint_state[0].position
        rospy.loginfo("Waiting for " + ARM_NAME + "joint states")
        arm_start = rospy.wait_for_message("/" + ARM_NAME + "/joint_states", JointState).position
        arm_end = grasp_response.arm_configuration
        env = self.scene_manager.get_environment_description()
        
        q_start = np.concatenate((arm_start, allegro_joints_position))
        #q_start = arm_start
        #q_end = np.concatenate((grasp_response.arm_configuration, allegro_joints_position))
        q_end = arm_end
        #print(len(q_start))
        #print(len(q_end))
        try:
            trajectory = self.moveit_planner.get_plan_to_config(q_end,q_start,env)
        except Exception as e:
            print(e)
            if trajectory is None:
                print("Generating trajectory failed.")
                return None, None, None

        #return arm_end, trajectory_response.trajectory.joint_trajectory, grasp_response.allegro_joint_state[0]
        return arm_end, trajectory, grasp_response.allegro_joint_state[0]

    def heuristic_grasp(self, request, hand_only, hand_client):
        number_of_grasp_candidates = 3 # 0 is top grasp, 1 & 2 are side grasps
        service_name = "gen_grasp_preshape"
        return self.get_reachable_grasp(request, number_of_grasp_candidates, service_name, hand_only, hand_client)

    def get_reachable_grasp(self, request, number_of_grasp_candidates, service_name, hand_only, hand_client):
        reachable_grasp_found = False
        i = 0
        allegro_joint_state = None
        max_attempts = 1 # 3 * 3
        attempts_tried = 0

        arm_js = rospy.wait_for_message("/" + ARM_NAME + "/joint_states", JointState)

        while not reachable_grasp_found and attempts_tried < max_attempts:
            grasp_response = call_service(service_name, request, GraspPreshape)
            possible_grasps_indices = np.arange(len(grasp_response.palm_goal_pose_world))
            np.random.shuffle(possible_grasps_indices)
            for idx in possible_grasps_indices:
                palm_pose_stamped = grasp_response.palm_goal_pose_world[idx] 
                allegro_joint_state = grasp_response.allegro_joint_state[idx]

                env = self.scene_manager.get_environment_description()
                trajectory_response = self.moveit_planner.get_plan_to_pose(
                        palm_pose_stamped.pose, # goal
                        arm_js.position + allegro_joint_state.position, # start
                        env
                        )
                if trajectory_response is None or not trajectory_response.success:
                    reachable_grasp_found = False
                else:
                    reachable_grasp_found = True
                    break
                i += 1
            attempts_tried += 1
        if hand_only:
            return None, allegro_joint_state
        if reachable_grasp_found:
            return trajectory_response.trajectory.joint_trajectory, allegro_joint_state
        else:
            return None, None
 
def fit_mesh_via_icp(mesh_path, obj_cloud_ros):
        rospy.loginfo("Fitting the mesh via ICP")
        service_name = "TableTop_PointCloud_Pose_Estimation"
        rospy.wait_for_service(service_name)
        req = rospy.ServiceProxy(service_name, MeshToCloudPose)
        resp = req(mesh_path, obj_cloud_ros, obj_cloud_ros)
        return resp.estimated_pose.pose

      
def active_learning_grasp(request):
    reachable_grasp_found = False

    while not reachable_grasp_found:
        response = call_service("grasp_active_plan", request, GraspPreshape)
        trajectory_response = plan_trajectory(None, response.arm_configuration)
        reachable_grasp_found = trajectory_response.success
    allegro_joint_state = response.allegro_joint_state

    return response.arm_configuration, trajectory_response.plan_traj, allegro_joint_state

def evaluate_current_grasp(hand_joint_state):
    INDEX_JOINT_ID = 0
    MIDDLE_JOINT_ID = 4
    RING_JOINT_ID = 8
    THUMB_JOINT_ID = 12
    assert "index" in hand_joint_state.name[INDEX_JOINT_ID] and\
           "middle" in hand_joint_state.name[MIDDLE_JOINT_ID] and\
           "ring" in hand_joint_state.name[RING_JOINT_ID] and\
           "thumb" in hand_joint_state.name[THUMB_JOINT_ID]
    req = PredictGraspSuccessRequest()
    req.index_joint_0  = hand_joint_state.position[INDEX_JOINT_ID]
    req.index_joint_1  = hand_joint_state.position[INDEX_JOINT_ID + 1]
    req.middle_joint_0 = hand_joint_state.position[MIDDLE_JOINT_ID]
    req.middle_joint_1 = hand_joint_state.position[MIDDLE_JOINT_ID + 1]
    req.ring_joint_0   = hand_joint_state.position[RING_JOINT_ID]
    req.ring_joint_1   = hand_joint_state.position[RING_JOINT_ID + 1]
    req.thumb_joint_0  = hand_joint_state.position[THUMB_JOINT_ID]
    req.thumb_joint_1  = hand_joint_state.position[THUMB_JOINT_ID + 1]
    response = call_service("predict_grasp_success", req, PredictGraspSuccess)
    return response.success_probability

def get_hand_gradients():
    response = call_service("get_fingers_configuration_gradient", FingersGradientRequest(), FingersGradient)
    return response.gradients 
