from geometry_msgs.msg import Pose, Quaternion, PoseStamped, PointStamped
import tf
import rospy

def get_pose_stamped_from_array(rpy_xyz, frame_id="/world"):
    pose_stamped = PoseStamped()
    pose_stamped.header.frame_id = frame_id

    # RPY to quaternion
    pose_quaternion = tf.transformations.quaternion_from_euler(\
                          rpy_xyz[0], rpy_xyz[1], rpy_xyz[2])
    pose_stamped.pose.orientation.x,\
    pose_stamped.pose.orientation.y,\
    pose_stamped.pose.orientation.z,\
    pose_stamped.pose.orientation.w = pose_quaternion 
    
    pose_stamped.pose.position.x,\
    pose_stamped.pose.position.y,\
    pose_stamped.pose.position.z = rpy_xyz[3:]
    
    return pose_stamped

def get_empty_stamped_pose():
    return get_pose_stamped_from_array([0.,0.,0.,0.,0.,0.])

def call_service(name, request, message_type, wait_for_service=False):
    response = None
    try: 
        if wait_for_service:
            wait_for_service(name)
        proxy = rospy.ServiceProxy(name, message_type)
        response = proxy(request)
    except Exception as e:
        rospy.logerr("Service " + name + " call failed: " + str(e))
    rospy.loginfo("Service " + name + " call finished successfully.")
    return response

def wait_for_service(service_name):
    rospy.loginfo("Waiting for service " + service_name)
    rospy.wait_for_service("Calling service" + service_name)
    rospy.loginfo("Waiting for service " + service_name + " finished successfully.")
