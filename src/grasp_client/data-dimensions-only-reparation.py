import pickle
import glob
import ast
import rospy
from scipy.spatial.transform import Rotation as R
import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir('tf_tools') + '/scripts')
from tf_client import TFClient
import tf
import time

def main():
    rospy.init_node("hello")
    listener = tf.TransformListener()
    paths = glob.glob(r'/mnt/data_collection_est_pose/*.txt')
    objects_info = pickle.load(open("objects_info.pkl", "rb"))
    used_properties = set(["palm_pose(est object frame)", "hand_config_preshape", "hand_config_after_closing", "success", "fingertips in contact", "reason", "est_obj_pose"])
    tf_client = TFClient()
    for i, path in enumerate(paths):
        if "last_index_spawned" in path:
            continue
        object_name = get_object_name(path)
        object_info = objects_info[object_name]
        with open(path, "r") as f:
            sample_raw = {}
            for line in f:
                if "=" not in line:
                    # parsing pose
                    chunks = line.split(":")
                    if len(chunks) != 2 or "orientation" in chunks[0]:
                        continue
                    sample_raw["est_obj_pose"].append(ast.literal_eval(chunks[1].strip()))
                    continue
                key, value = line.split("=")
                if key in used_properties:
                    if "palm_pose" in key:
                        parsed_value = ast.literal_eval(value)
                    elif "est_obj_pose" in key:
                        parsed_value = []
                    else:
                        parsed_value = value
                    sample_raw[key] = parsed_value

            sample_raw["width"] = object_info["width"]
            sample_raw["height"] = object_info["height"]
            sample_raw["depth"] = object_info["depth"]

            #save_data(path, sample_raw)
            print("{:.4f}".format(i * 100.0 / len(paths)) + "% finished.")
            tf_client.delete_all_frames()

def parse_object_pose(value_string):
    print(value_string)

def save_data(original_path, sample_raw):
    path = original_path.replace("data_collection_est_pose", "data_collection_est_pose_updated_dim")
    fh = open(path, "w")
    string = ""
    string += "est_obj_pose=" + str(sample_raw["est_obj_pose"]) + "\n"
    string += "object width=" + "{:.4f}".format(sample_raw["width"]) + "\n"
    string += "object height=" + "{:.4f}".format(sample_raw["height"]) + "\n"
    string += "object depth=" + "{:.4f}".format(sample_raw["depth"]) + "\n"
    string += "palm_pose(est object frame)=" + str(sample_raw["palm_pose(est object frame)"]) + "\n"
    string += "hand_config_preshape=" + sample_raw["hand_config_preshape"] 
    string += "hand_config_after_closing=" + sample_raw["hand_config_after_closing"] 
    string += "fingertips in contact=" + sample_raw["fingertips in contact"]
    string += "success=" + sample_raw["success"]
    string += "reason=" + sample_raw["reason"]  
    fh.write(string)
    fh.close()

def get_object_name(path):
    filename = path.split("/")[-1]
    object_name = filename.split("-")[0]
    return object_name

if __name__ == '__main__':
    main()
