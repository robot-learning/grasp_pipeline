#!/usr/bin/env python
import os
import rospy
import time
import trimesh
import tf
import numpy as np
import roslib
import sys
from grasp_sim_client import GraspClient 

# for visualization (debugging)
sys.path.append(roslib.packages.get_pkg_dir('tf_tools') + '/scripts')
from tf_broadcaster import TFBroadcaster

def main():
    dc_client = GraspClient()
    listener = tf.TransformListener()
    #dataset_dir = '/mnt/tars_data/sim_dataset/BigBird/BigBird_mesh'
    dataset_name = 'BigBird'
    #dataset_name = 'GraspDatabase'
    dataset_dir = '/mnt/tars_data/sim_dataset/' + dataset_name \
                    + '/' + dataset_name + '_mesh'
    object_mesh_dirs = os.listdir(dataset_dir)
    # Reverse object mesh directory list
    # object_mesh_dirs = object_mesh_dirs[::-1]
    # dc_client.last_object_name = 'empty'
    # print object_mesh_dirs

    #object_mesh_dirs = ['pringles_bbq'] 

    # Bigbird objects that don't work for gazebo+dart.
    bigbird_objects_exclude = {'coca_cola_glass_bottle', 'softsoap_clear',
                       'softsoap_green', 'softsoap_purple', 'windex',
                       'palmolive_orange', 'palmolive_green', 
                       #'progresso_new_england_clam_chowder', 
                       'listerine_green', 'hersheys_bar', 'mahatma_rice', 
                       'paper_plate'}
    #exclude_object = 'windex'
    skip_object = True
    dc_client.move_robot_home()
    grasp_failure_retry_times = dc_client.num_grasps_per_object
    for i, object_name in enumerate(object_mesh_dirs):
        rospy.loginfo('Object: %s' %object_name)

        dc_client.get_last_object_id_name() # update last object info
        # Resume from the last object.
        if dc_client.last_object_name == 'empty' or \
                object_name == dc_client.last_object_name:
            skip_object = False
        if skip_object:
            continue

        if dataset_name == 'BigBird' and object_name in bigbird_objects_exclude:
            continue

        if dataset_name == 'BigBird':
            obj_mesh_path = dataset_dir + '/' + object_name + \
                            '/textured_meshes/optimized_tsdf_textured_mesh.stl'
            #obj_mesh_path = dataset_dir + '/' + object_name + \
            #                '/meshes/poisson_proc'
        elif dataset_name == 'GraspDatabase':
            obj_mesh_path = dataset_dir + '/' + object_name + \
                            '/' + object_name + '_proc.stl' 

        object_pose_array = [0., 0., 0., 0., -0.8, 0.59]
        object_pose_stamped = dc_client.get_pose_stamped_from_array(object_pose_array) # is this client related?
        dc_client.update_gazebo_object_client(object_name, object_pose_array, object_model_name=object_name) #update_sim_scene
 
        dc_client.set_up_object_name(object_name=object_name, object_mesh_path=obj_mesh_path)
        dc_client.control_allegro_config_client(go_home=True)
        dc_client.move_robot_to_object_heuristic()
        #dc_client.move_robot_to_table()

        reached = False;
        raw_input("press enter to start online gradient descent")
        object_frame_name = object_name + "__" + object_name + "_link"
	object_representation = trimesh.load(obj_mesh_path)
	step_size = 10
        valid_set = []
        online_gradient_descent(valid_set, step_size, object_representation, object_frame_name, dc_client.hand_client, listener)
        print("Gradient descent finished!")
        raw_input("press enter to end")
        while not reached and not rospy.is_shutdown():
	    # get closest point to index tip on object's surface (in object frame)
	    try:
		trans, rot = listener.lookupTransform(object_frame_name, "index_tip", rospy.Time(0))
	    except Exception as e:
		print(e)

	    mesh = trimesh.load(obj_mesh_path)
	    fingertip_location = trans
	    # this points are in object's frame
	    closest_points, distances, _ = mesh.nearest.on_surface([fingertip_location])
	    point_in_object_frame = closest_points[0] # this is 100% correct
	    #dc_client.generate_point_on_surface(point_in_object_frame)

	    # convert the point to palm link frame
	    try:
		trans, rot = listener.lookupTransform("palm_link", object_frame_name, rospy.Time(0))
	    except Exception as e:
		print(e)
	    rot_matrix = tf.transformations.quaternion_matrix(rot)
	    point_in_object_frame = np.matrix(np.append(point_in_object_frame, 1)).transpose()
     
	    result = (rot_matrix * point_in_object_frame).flatten().tolist()[0][:3] + np.array(trans)
     
	    result = [float(v) for v in result]
	    print(result) 
            
	    raw_input("press enter to continue")
	    reached = dc_client.close_hand([result])
	    raw_input("press enter to continue")

def online_gradient_descent(valid_set, step_size, object_representation, object_frame_name, hand_client, listener):
    q_index, q_middle, q_ring, q_thumb = get_current_state(hand_client)
    fingers_in_contact = [False, False, False, False] # index, middle, ring, thumb
    i = 0
    visualization = TFBroadcaster()
    while not all(fingers_in_contact):
       q_index, q_middle, q_ring, q_thumb = move_to(q_index, q_middle, q_ring, q_thumb, hand_client) 

       # object frame
       fk = forward_kinematics(listener, object_frame_name)
       closest_points_obj_frame = get_closest_points(object_representation, fk) 

       # palm link frame
       fk = forward_kinematics(listener, "palm_link")
       closest_points = convert_to_palm_frame(closest_points_obj_frame, listener, object_frame_name)

       visualize_points(closest_points, visualization)
       #raw_input("Visualizing closest points.")

       fingers_in_contact = hand_client.get_biotac_contact_readings()
       print("fingers in contact: " + str(fingers_in_contact))

       g_index, g_middle, g_ring, g_thumb = get_gradient(fk, closest_points) # must be in palm link frame because J is in palm link frame
       y_index, y_middle, y_ring, y_thumb = update_step(q_index, q_middle, q_ring, q_thumb,\
                                                        g_index, g_middle, g_ring, g_thumb,\
                                                        step_size, hand_client)
       q_index_to_go, q_middle_to_go, q_ring_to_go, q_thumb_to_go = project(y_index, y_middle, y_ring, y_thumb)

       # go to new joint state only if finger is not in contact
       if not fingers_in_contact[0]:
           q_index = q_index_to_go
       if not fingers_in_contact[1]:
           q_middle = q_middle_to_go 
       if not fingers_in_contact[2]:
           q_ring = q_ring_to_go
       if not fingers_in_contact[3]:
           q_thumb = q_thumb_to_go

def visualize_points(points, visualization):
    points_for_visualization = []
    for i in range(points.shape[1]):
        point = points[:,i]
        points_for_visualization.append(np.asarray(point))
    visualization.update_points(points_for_visualization)
    
def project(y_index, y_middle, y_ring, y_thumb):
    q_index = project_finger(y_index)
    q_middle = project_finger(y_middle)
    q_ring = project_finger(y_ring)
    q_thumb = project_thumb(y_thumb)

    return q_index, q_middle, q_ring, q_thumb

def project_finger(q):
    q[0][0] = max(q[0][0], -0.571) # lower bound
    q[0][0] = min(q[0][0], 0.571) # upper bound

    q[1][0] = max(q[1][0], -0.296)
    q[1][0] = min(q[1][0], 1.711)

    q[2][0] = max(q[2][0], -0.274)
    q[2][0] = min(q[2][0], 1.809)

    q[3][0] = max(q[3][0], -0.327)
    q[3][0] = min(q[3][0], 1.718)
  
    return q

def project_thumb(q):
    q[0][0] = max(q[0][0], 0.363) # lower bound
    q[0][0] = min(q[0][0], 1.496) # upper bound

    q[1][0] = max(q[1][0], -0.205)
    q[1][0] = min(q[1][0], 1.263)

    q[2][0] = max(q[2][0], -0.289)
    q[2][0] = min(q[2][0], 1.744)

    q[3][0] = max(q[3][0], -0.262)
    q[3][0] = min(q[3][0], 1.819)
  
    return q

def update_step(q_index, q_middle, q_ring, q_thumb,\
                g_index, g_middle, g_ring, g_thumb,\
                step_size, hand_client):
    j_index, j_middle, j_ring, j_thumb = get_translational_jacobians(hand_client)

    y_index = q_index - step_size * np.dot(j_index.T, g_index)
    y_middle = q_middle - step_size * np.dot(j_middle.T, g_middle)
    y_ring = q_ring - step_size * np.dot(j_ring.T, g_ring)
    y_thumb = q_thumb - step_size * np.dot(j_thumb.T, g_thumb)

    assert y_index.shape == (4,1)

    return y_index, y_middle, y_ring, y_thumb

def get_translational_jacobians(hand_client):
    j_index, j_middle, j_ring, j_thumb = hand_client.get_jacobians()
    return j_index[:3,:], j_middle[:3,:], j_ring[:3,:], j_thumb[:3, :]

def move_to(q_index, q_middle, q_ring, q_thumb, hand_client):
    q1 = q_index.flatten().tolist()[0]
    q2 = q_middle.flatten().tolist()[0]
    q3 = q_ring.flatten().tolist()[0]
    q4 = q_thumb.flatten().tolist()[0]
    
    q =  q1 + q2 + q3 + q4 # '+' here is concatenation

    assert len(q) == 16, q # q are joints for all fingers

    hand_client.send_pos_cmd(q)
    return get_current_state(hand_client)

def get_current_state(hand_client):
    joint_state_msg = hand_client.get_joint_state()

    q_index = np.matrix(joint_state_msg.position[0:4]).T
    q_middle = np.matrix(joint_state_msg.position[4:8]).T
    q_ring = np.matrix(joint_state_msg.position[8:12]).T
    q_thumb = np.matrix(joint_state_msg.position[12:16]).T
    
    assert q_index.shape == (4,1), q_index.shape # column vector
   
    return (q_index, q_middle, q_ring, q_thumb)

def send_cmd(q, hand_client):
    # convert from numpy to flat array
    flatten = q.flatten('F') # 'F' because it is a finger per column
    des_js = flatten.tolist()

def forward_kinematics(listener, frame_name):
    # return positions of fingertips in frame_name
    positions = np.zeros((3,4)) # 4 fingers (cols), x,y,z (rows)
    fingers = ["index_tip", "middle_tip", "ring_tip", "thumb_tip"]
    try:
        for i,finger in enumerate(fingers):
            trans, rot = listener.lookupTransform(frame_name, finger, rospy.Time(0))
            positions[:,i] = trans
    except Exception as e:
	print(e)

    return positions

def check_contact(distances):
    # assume object is mesh
    result = [False, False, False, False]
    for i, distance in enumerate(distances):
        if distance < 1e-3:
            result[i] = True
    return result

def get_closest_points(object_representation, points): 
    # object frame
    closest_points, distances, _ = object_representation.nearest.on_surface(points.T) 
    return closest_points

def get_gradient(current_positions, goal_positions):
    assert current_positions.shape == (3,4), current_positions # 3 rows: x,y,z; 4 columns: one per finger
    assert goal_positions.shape == (3,4), goal_positions # 3 rows: x,y,z; 4 columns: one per finger

    distance_index = np.tanh(np.matrix(goal_positions[:,0] - current_positions[:,0]).T)
    distance_middle = np.tanh(np.matrix(goal_positions[:,1] - current_positions[:,1]).T)
    distance_ring = np.tanh(np.matrix(goal_positions[:,2] - current_positions[:,2]).T)
    distance_thumb = np.tanh(np.matrix(goal_positions[:,3] - current_positions[:,3]).T)
 
    assert distance_thumb.shape == (3,1), distance_thumb.shape

    return -distance_index, -distance_middle, -distance_ring, -distance_thumb

def convert_to_palm_frame(points, listener, object_frame_name):
    # convert a point from object frame to palm link frame
    try:
        trans, rot = listener.lookupTransform("palm_link", object_frame_name, rospy.Time(0))
        trans = np.matrix(trans).T
    except Exception as e:
	print(e)
    rot_matrix = tf.transformations.quaternion_matrix(rot)

    result = np.zeros((3,4)) # every point is xyz, hence 3 rows, there is 4 fingers, hence 4 columns
    for i, point_in_object_frame in enumerate(points):
        point_in_object_frame = np.matrix(np.append(point_in_object_frame, 1)).T
        point_in_palm_frame = np.dot(rot_matrix,  point_in_object_frame)[:3,0] + trans
        result[:,i] = np.asarray(point_in_palm_frame).ravel()
    return result

if __name__ == '__main__':
    main()
