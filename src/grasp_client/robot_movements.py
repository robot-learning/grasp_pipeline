import rospy
import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("grasp_pipeline")  + "/script")
import robot_traj_interface
from geometry_msgs.msg import Pose, PoseStamped
import tf
import numpy as np
from grasp_pipeline.srv import *
from utils import call_service
sys.path.append(rp.get_pkg_dir("ll4ma_util")+"/src/ll4ma_util/")
from ros_util import get_pose_stamped_msg
from scipy.spatial.transform import Rotation as R
import numpy as np

ARM_NAME = "lbr4"

def move_robot_home(move_it_planner, hand_client, scene_manager = None):
    if move_it_planner:
        print("moving arm home..")
        if scene_manager is not None:
            env = scene_manager.get_environment_description(remove_object=True, shorter_box=True)
        else:
            env = {}
        target_q = target_config = [0.0 for i in range(7)]
        trajectory = move_it_planner.get_plan_to_config(target_config = target_q, environment = env, ignore_hand=True)
        while trajectory is None:
            trajectory = move_it_planner.get_plan_to_config(target_q, ignore_hand=True)

        move_arm(trajectory=trajectory)

    print("moving hand home..")
    if hand_client is not None:
        hand_home = [0, 0, 0, 0,
                     0, 0, 0, 0,
                     0, 0, 0, 0.,
                     0.38, 0, 0, 0]
        #THUMB_JOINT_0_ID = 12
        #hand_home[THUMB_JOINT_0_ID] = 1.45 # thumb opposing fingers
        hand_client.send_pos_cmd(hand_home, contact_sensing = False)


def move_arm_to_config(arm_q,move_it_planner):
    print("moving arm home..")
    trajectory = move_it_planner.get_plan_to_config(target_config = arm_q, environment = {}, ignore_hand=True)
    while trajectory is None:
        trajectory = move_it_planner.get_plan_to_config(target_q, ignore_hand=True)
    move_arm(trajectory=trajectory)


def move_straight_forward():
    # 1. get the current palm pose
    listener = tf.TransformListener()
    listener.waitForTransform("world", "palm_link", rospy.Time(0), rospy.Duration(5.0))
    position, orientation = listener.lookupTransform("world", "palm_link", rospy.Time(0))

    # 2. call move_arm to current position + 30cm in x direction 
    position[0] += 0.30 
    goal_pose = Pose()
    goal_pose.position.x = position[0]
    goal_pose.position.y = position[1]
    goal_pose.position.z = position[2]
    goal_pose.orientation.x = orientation[0]
    goal_pose.orientation.y = orientation[1]
    goal_pose.orientation.z = orientation[2]
    goal_pose.orientation.w = orientation[3]

    # task space controller
    print("is task space controller running?")
    rospy.wait_for_service("/lbr4/task_position_service")
    srv_call = rospy.ServiceProxy("/lbr4/task_position_service", TaskPosition)
    print("yes, awesome! :)")
    req = TaskPositionRequest()
    pose = PoseStamped()
    pose.header.frame_id = "world"
    pose.pose = goal_pose
    req.cmd_pose = pose
    input("press enter to send command to task space controller")
    return srv_call(req)

'''
def move_palm_to_pose(palm_pose_world, arm_kdl = None):
    # uses task space controller in the end
    #rospy.wait_for_service("/lbr4/task_position_service")
    #srv_call = rospy.ServiceProxy("/lbr4/task_position_service", TaskPosition)
    if arm_kdl is None:
        arm_kdl = HandArmModel("robot_description", base_link="world")
    
    final_pose_reachable = plan_trajectory(palm_pose_world).success
    if not final_pose_reachable:
        rospy.logwarn("The goal pose is not safely reachable.")
        return

    # planner
    traj, target_pose, distance, T_pf_to_world = get_traj_for_planner(palm_pose_world, arm_kdl)
    offset = np.zeros((4,1)) # offset in palm link w.r.t. original goal
    offset[0,0] = 0.1 # in meters, positive means toward the object
    offset[3,0] = 1
    safely_reachable = False
    controller_pose = Pose()
    controller_pose.orientation = palm_pose_world.orientation

    controller_pose_homogeneus = np.dot(T_pf_to_world, offset)
    controller_pose.position.x = controller_pose_homogeneus[0,0]
    controller_pose.position.y = controller_pose_homogeneus[1,0]
    controller_pose.position.z = controller_pose_homogeneus[2,0]

    move_arm(trajectory=traj)

    #print("(world) planner pose: ", target_pose)
    #print("(world) task space controller pose: ", controller_pose)
    #print("(world) original goal : ", palm_pose_world)

    # task space controller
    req = TaskPositionRequest()
    pose = PoseStamped()
    pose.header.frame_id = "world"
    pose.pose = controller_pose
    req.cmd_pose = pose
    input("press enter to use task space controller")
    return srv_call(req)
'''

def move_arm(palm_pose=None, go_home=False, trajectory=None):
    if trajectory is None:
        response = plan_trajectory(palm_pose, None, go_home)
        print("response: ")
        print(response)
        if not response.success:
            rospy.logwarn("Trajectory planning ended unsuccessfully.")
            return False
        trajectory = response.plan_traj

    trajectory_manager = robot_traj_interface.robotTrajInterface(arm_prefix=ARM_NAME, init_node=False)
    input("press enter to execute arm trajectory")
    execute_arm_trajectory(trajectory, trajectory_manager)
    rospy.loginfo("Arm successfully moved.")
    return True

def move_arm_up(moveit_planner, scene_manager):
    rospy.loginfo("moving arm up")
    listener = tf.TransformListener()
    listener.waitForTransform("world", "palm_link", rospy.Time(0), rospy.Duration(5.0))
    position, orientation = listener.lookupTransform("world", "palm_link", rospy.Time(0))
    current_pose = get_pose_stamped_msg(position, orientation)
    target_pose = current_pose.pose
    target_pose.position.z += 0.1

    #arm_q = [0.0, 1.05, 0.0, 0.0, 0.0, 0.0, 0.0]
 
    env = scene_manager.get_environment_description(remove_object=True, shorter_box=True)
    for i in range(2):
        try:
            #trajectory = moveit_planner.get_plan_to_config(arm_q, environment=env, ignore_hand=True)
            trajectory = moveit_planner.get_plan_to_pose(target_pose, cartesian_path=True)
            move_arm(trajectory=trajectory)
            break
        except Exception as e:
            print(e)
            continue

def plan_trajectory(palm_pose=None, arm_configuration=None):
    raise ValueError("Deprecated")
    arm_service_name = "moveit_cartesian_pose_planner"
    request = PalmGoalPoseWorldRequest()
    if arm_configuration is not None:
        request.go_to_configuration = True
        request.arm_configuration = arm_configuration
    elif palm_pose is not None: 
        request.palm_goal_pose_world = palm_pose
    rospy.loginfo("Calling service: " + arm_service_name)
    return call_service(arm_service_name, request, PalmGoalPoseWorld)

def get_traj_for_planner(preshape_pose_world, arm_kdl, task_space_controller_distance = 0.005):
    '''
    This expects the task space controller to complete the movement.
    '''
    # pose that the planner should reach is NOT the preshape pose
    target_pose = Pose()
    target_pose.position.x = preshape_pose_world.position.x
    target_pose.position.y = preshape_pose_world.position.y
    target_pose.position.z = preshape_pose_world.position.z
    target_pose.orientation.x = preshape_pose_world.orientation.x
    target_pose.orientation.y = preshape_pose_world.orientation.y
    target_pose.orientation.z = preshape_pose_world.orientation.z
    target_pose.orientation.w = preshape_pose_world.orientation.w

    q = get_ik(preshape_pose_world).arm_q_ik
    T_pf_to_world = arm_kdl.FK_arm(q)
    v = np.zeros((4,1))
    v[0,0] = - task_space_controller_distance # in meters
    v[3,0] = 1
    safely_reachable = False
    while not safely_reachable:
        v[0,0] += 0.005
        planner_pose = np.dot(T_pf_to_world, v)
        target_pose.position.x = planner_pose[0,0]
        target_pose.position.y = planner_pose[1,0]
        target_pose.position.z = planner_pose[2,0]
        response = plan_trajectory(target_pose)
        safely_reachable = response.success
        traj = response.plan_traj
        if v[0,0] > 0:
            raise Exception("Can't move to the desired pose.")
    print("task space distance (predicted): ", v[0,0])
    return traj, target_pose, -v[0,0], T_pf_to_world


def get_ik(palm_pose):
    arm_service_name = "moveit_cartesian_pose_planner"
    request = PalmGoalPoseWorldRequest()
    request.palm_goal_pose_world = palm_pose
    request.ik_only = True
    return call_service(arm_service_name, request, PalmGoalPoseWorld)

def execute_arm_trajectory(trajectory, trajectory_manager, send_cmd_manually=False):
    rospy.loginfo("Executing arm trajectory")
    send_trajectory = True

    if send_cmd_manually:
        send_cmd = input("Send to robot? (y/n)")
        if send_cmd != 'y':
            send_trajectory = False

    if send_trajectory:
       trajectory_manager.send_jtraj(trajectory) 
