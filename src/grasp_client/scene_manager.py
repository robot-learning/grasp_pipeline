import rospy
import os
import numpy as np
from scipy.spatial.transform import Rotation
from utils import call_service, get_empty_stamped_pose 
from grasp_pipeline.srv import *
from geometry_msgs.msg import Pose
from std_msgs.msg import ColorRGBA
from moveit_msgs.msg import CollisionObject

# apparently these don't work for gazebo+dart
bigbird_objects_exclude = {'coca_cola_glass_bottle', 'softsoap_clear',
                       'softsoap_green', 'softsoap_purple', 'windex',
                       'palmolive_orange', 'palmolive_green', 
                       'listerine_green', 'hersheys_bar', 'mahatma_rice', 
                       'paper_plate', 'quaker_chewy_peanut_butter_chocolate_chip_link'}

ycb_objects_exclude = {'065-c_cups'}

'''
    This class is responsible for handling scene objects management in gazebo and moveit
'''


class SceneManager():
    def __init__(self, simulation=True, dataset_name = "BigBird", dataset_collection = False):
        self.simulation = simulation
        self.moveit_scene_clean = True
        self.object_representation = None
        self.object_name = "real_object_name"
        if self.simulation:
            self.dataset_dir = "/mnt/tars_data/sim_dataset/" + dataset_name + \
                               "/" + dataset_name + "_mesh"
            self.object_mesh_dirs = os.listdir(self.dataset_dir)
            if dataset_collection:
                fh = open("/mnt/data_collection/last_index_spawned.txt", "r")
                self.object_index = int(fh.read())
                fh.close()
            else:
                self.object_index = 0
            self.dataset_name = dataset_name
            self.update_simulation_scene()
        self.object_pose = None
        
    def update_simulation_scene(self):
        self.update_current_object_info()
        return self.update_gazebo_scene()

    def repeat_simulation_scene(self):
        self.update_gazebo_scene()

    def update_object_representation(self, object_representation):
        self.object_representation = object_representation

    def update_current_object_info(self):
        self.object_name = self.object_mesh_dirs[self.object_index]
        while self.object_name in bigbird_objects_exclude or\
              self.object_name in ycb_objects_exclude:
            self.update_object_index()
            self.object_name = self.object_mesh_dirs[self.object_index]
        self.object_representation.mesh_path = self.dataset_dir + "/" + self.object_name + "/"
        if self.dataset_name == "BigBird":
            #self.object_mesh_path += "/textured_meshes/optimized_tsdf_textured_mesh.stl"
            self.object_representation.mesh_path += "/meshes/poisson_proc.stl"
        else:
            self.object_representation.mesh_path += self.object_name + "_proc.stl"
 
        self.object_pose = Pose()
        self.object_pose.position.x = 0
        self.object_pose.position.y = -0.8
        self.object_pose.position.z = 0.59
        roll = 0.0
        pitch = 0.0
        yaw = np.random.rand() * 2 * np.pi
        quat = Rotation.from_euler('xyz', [roll,pitch,yaw], degrees=False).as_quat()
        self.object_pose.orientation.x = quat[0]
        self.object_pose.orientation.y = quat[1]
        self.object_pose.orientation.z = quat[2]
        self.object_pose.orientation.w = quat[3]
        self.object_pose_array = [roll,pitch,yaw, 0.,-0.8,0.59]
 
        fh = open("/mnt/data_collection/last_index_spawned.txt", "w")
        fh.write(str(self.object_index))
        fh.close()
        self.update_object_index()
        rospy.loginfo("Current object info updated.")

    def update_object_index(self):
        self.object_index += 1
        self.object_index %= len(self.object_mesh_dirs)

############ gazebo #####################

    def update_gazebo_scene(self):
        request = UpdateObjectGazeboRequest()
        request.object_name = self.object_name
        request.object_pose_array = self.object_pose_array
        request.object_model_name = self.object_name
        return call_service("update_gazebo_object", request, UpdateObjectGazebo)

    def get_true_object_pose(self, listener):
        object_frame_name = self.object_name + "__" + self.object_name + "_link"
        listener.waitForTransform("world", object_frame_name, rospy.Time(), rospy.Duration(4.0))
        now = rospy.Time.now()
        listener.waitForTransform("world", object_frame_name, now, rospy.Duration(4.0))
        return listener.lookupTransform("world", object_frame_name, now)



############# moveit ####################

    def get_environment_description(self, remove_object=False, shorter_box=False, object_as_box=True):
        box = get_box_description(self.simulation, shorter_box)
        if self.object_representation is None:
            return {"box": box}
        obj = self.get_obj_description(remove_object, object_as_box)
        return {"box": box, "obj": obj}

    def update_object_representation(self, object_representation):
        self.object_representation = object_representation

    def get_obj_description(self, remove_object, object_as_box):
        '''
        Assumption: pose is in the world frame.
        '''
        color = ColorRGBA()
        color.b = 1
        obj_pose = self.object_representation.pose
        if object_as_box:
            obj = {
                    "frame_id" : "world",
                    "object_type" : "box",
                    "pose" : obj_pose,
                    "color" : color,
                    "extents" : self.object_representation.get_bounding_box()
                  }
        else:
            mesh_path = self.object_representation.mesh_path
            obj = {
                    "frame_id" : "world",
                    "object_type" : "mesh",
                    "path": mesh_path,
                    "pose" : obj_pose,
                    "color": color
                  }
        if remove_object:
            obj["operation"] = CollisionObject.REMOVE
        else:
            obj["operation"] = CollisionObject.ADD
        return obj



def get_box_description(simulation, shorter_box):
    if simulation:
        box_pose = Pose()
        box_pose.position.x = 0.
        box_pose.position.y = -0.8
        box_pose.position.z = 0.295
        if shorter_box:
            # when robot is in contact with the box and wants to move up
            box_pose.position.z -= 0.1
        box_pose.orientation.x = 0.
        box_pose.orientation.y = 0.
        box_pose.orientation.z = 0.
        box_pose.orientation.w = 1.
        color = ColorRGBA()
        color.b = 1
        box = {
            "frame_id" : "world",
            "object_type" : "box",
            "extents" : [0.9125,0.61,0.59],
            "pose" : box_pose,
            "operation": 0, # ADD,
            "color": color
          }
    else:
        box_pose = Pose()
        box_pose.position.x = 0.
        box_pose.position.y = -0.88
        #box_pose.position.z = 0.59 + 0.015 # brown table
        box_pose.position.z = 0.73 + 0.015 # white table
        if shorter_box:
            # when robot is in contact with the box and wants to move up
            box_pose.position.z -= 0.1
        box_pose.orientation.x = 0.
        box_pose.orientation.y = 0.
        box_pose.orientation.z = 0.
        box_pose.orientation.w = 1.
        color = ColorRGBA()
        color.b = 1
        box = {
            "frame_id" : "world",
            "object_type" : "box",
            "extents" : [1.4, 0.9, 0.015],
            "pose" : box_pose,
            "operation": 0, # ADD,
            "color": color
        }
    return box


'''
    def update_moveit_scene(self, mesh_path, pose):
        if not self.moveit_scene_clean:
            self.clean_moveit_scene()
        self.create_moveit_scene(mesh_path, pose)
        self.moveit_scene_clean = False

    def clean_moveit_scene(self):
        request = ManageMoveitSceneRequest()
        request.clean_scene = True
        call_service("clean_moveit_scene", request, ManageMoveitScene)

    def create_moveit_scene(self, mesh_path, pose):
        rospy.loginfo("Creating moveit scene with pose..")
        rospy.loginfo(pose)
        request = ManageMoveitSceneRequest()
        request.create_scene = True
        request.object_mesh_path = mesh_path

        stamped_pose = get_empty_stamped_pose()
        stamped_pose.pose = pose

        request.object_pose_world = stamped_pose
        call_service("create_moveit_scene", request, ManageMoveitScene)
        
'''
