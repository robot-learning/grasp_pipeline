#!/usr/bin/env python
import os
import rospy
import time
import trimesh
import tf
import numpy as np
import roslib
import sys
from grasp_pipeline.srv import *
from robot_movements import move_palm_to_pose, move_robot_home, move_arm_up, move_arm
from grasp_interface import GraspClientInterface
from geometry_msgs.msg import Pose, PoseStamped
from sensor_msgs.msg import Image
from pypcd import PointCloud
from cv_bridge import CvBridge
import cv2
import time
from datetime import datetime

def main():
    grasp_interface = GraspClientInterface(simulation = True, reconstruct_mesh = True)
    bridge = CvBridge()

    counter = 0
    trajectory=None
    while True:
        # 113 objects, 5 trials, 2 methods
        if counter == 113 * 5 * 2:
            break

        move_robot_home(grasp_interface.moveit_planner,grasp_interface.hand_client, grasp_interface.scene_manager)
        if counter % 2 == 0:
            grasp_interface.scene_manager.update_simulation_scene()
        else:
            grasp_interface.scene_manager.repeat_simulation_scene()
        time.sleep(2)
        
        # step 1: get current pose of the object
        start_transl, start_orient = grasp_interface.get_true_object_pose()
        start_transl_2, start_orient_2 = grasp_interface.get_true_object_pose()

        rospy.loginfo("[experiment info] starting experiment")
        total_experiment_info_str = ""
        for i in range(len(start_transl)):
            if abs(start_transl_2[i] - start_transl[i]) > 1e-3:
                print(start_transl_2)
                print(start_transl)
                #raise Exception("object pose is not deterministic!?")

        for i in range(len(start_orient)):
            if abs(start_orient_2[i] - start_orient[i]) > 1e-3:
                print(start_orient_2)
                print(start_orient)
                #raise Exception("object pose is not deterministic!?")
        rospy.loginfo("object pose is deterministic")

        # step 2: move to preshape
        if counter % 2 == 0:
            print("planning preshape...")
            object_info, object_id, trajectory, hand_q = grasp_interface.move_to_preshape()
            raw_input("press enter to continue")
            if trajectory is None:
                print("planning trajectory failed, moving to next object")
                counter+= 2
                continue 
        grasp_interface.hand_client.send_pos_cmd(hand_q, False)
        move_arm(trajectory=trajectory)

        time_now = datetime.now().strftime("%m-%d-%Y--%H-%M-%S")
        preshape_image = get_image(bridge)
        cv2.imwrite("/home/mmatak/catkin_ws/src/grasp_pipeline/src/grasp_client/results/" + str(object_id) + "-" + time_now + "-preshape" + ".jpeg", preshape_image)

        line = "object id: " + str(object_id)
        rospy.loginfo("[experiment info] " + line)
        total_experiment_info_str += line + "|"
        # step 3: evaluate preshape classifier

        position, orientation = grasp_interface.get_current_palm_pose_in_object_frame()
        
        palm_pose = Pose()
        palm_pose.orientation.x,\
        palm_pose.orientation.y,\
        palm_pose.orientation.z,\
        palm_pose.orientation.w = orientation
 
        palm_pose.position.x,\
        palm_pose.position.y,\
        palm_pose.position.z = position

        palm_pose_msg = PoseStamped()
        palm_pose_msg.pose = palm_pose
        palm_pose_msg.header.frame_id = "estimated_object_pose"
        hand_config = grasp_interface.hand_client.get_joint_state()
        
        service_name = "evaluate_grasp"
        proxy = rospy.ServiceProxy(service_name, GraspInfo)
        request = GraspInfoRequest()
        request.obj = object_info
        request.palm_pose_obj_frame = palm_pose_msg
        request.full_allegro_joint_state = hand_config
        rospy.wait_for_service(service_name)
        grasp_response = proxy(request)
        line = "Preshape evaluation: "
        rospy.loginfo("[experiment info] " + line)
        rospy.loginfo(grasp_response)

        # step 4: check whether the object moved
        next_transl, next_orient = grasp_interface.get_true_object_pose()

        object_moved = False
        for i in range(len(start_transl)):
            if abs(next_transl[i] - start_transl[i]) > 1e-2:
                object_moved = True
        for i in range(len(start_orient)):
            if abs(next_orient[i] - start_orient[i]) > 0.1:
                object_moved = True
        line = "Object moved: " + str(object_moved)
        rospy.loginfo("[experiment info] " + line)
        total_experiment_info_str += line + "|"
        if object_moved:
            rospy.loginfo("[experiment info] start transl: " + str(start_transl))
            rospy.loginfo("[experiment info] end transl: " + str(next_transl))
            rospy.loginfo("[experiment info] start orient : " + str(start_orient))
            rospy.loginfo("[experiment info] end orient: " + str(next_orient))

        # visualize new state 
        rospy.wait_for_service("update_robot_state")
        proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
        proxy(UpdateRobotStateRequest())

 
        # step 5: close hand and evaluate classifiers

        method_used = None
        if counter%2 == 0:
            method_used = "GN"
            grasp_interface.close_hand(gauss_newton=True)
        else:
            method_used = "heuristic"
            grasp_interface.close_hand(heuristic=True)

        rospy.loginfo("hand closed")

        time_now = datetime.now().strftime("%m-%d-%Y--%H-%M-%S")
        image = get_image(bridge)
        cv2.imwrite("/home/mmatak/catkin_ws/src/grasp_pipeline/src/grasp_client/results/" + str(object_id) + "-" + time_now + "-hand-closed-" + method_used + ".jpeg", image)

        hand_config_in_contact = grasp_interface.hand_client.get_joint_state()

        service_name = "evaluate_grasp"
        proxy = rospy.ServiceProxy(service_name, GraspInfo)
        request.full_allegro_joint_state = hand_config_in_contact
        rospy.loginfo("waiting for service..")
        rospy.wait_for_service(service_name)
        rospy.loginfo("Evaluating grasp..")
        grasp_response = proxy(request)
        line = "Grasp evaluated: "
        rospy.loginfo("[experiment info]" + line)
        rospy.loginfo(grasp_response)

        #rospy.loginfo("[experiment info] Fingertips in contact: ")
        #rospy.loginfo(fingertips_in_contact)

        try:
            move_arm_up(grasp_interface.moveit_planner, grasp_interface.scene_manager)
        except Exception as e:
            line = "result: 0, moving the arm up failed"
            rospy.loginfo("[experiment info] " + line)
            total_experiment_info_str += line + "|"
            counter += 2
            continue

        # step 6: get final label
        final_transl, final_orient = grasp_interface.get_true_object_pose()

        final_z = final_transl[2]
        initial_z = start_transl[2]
        success = final_z > initial_z + 0.05
        if success is False:
            line = "result: 0, object not high enough, method used: " + method_used
        else:
            line = "result: 1, successful lift! method used: " + method_used

        rospy.loginfo("[experiment info] " + line)
        total_experiment_info_str += line + "|"
        line = "experiment finished"
        rospy.loginfo("[experiment info] " + line)
        total_experiment_info_str += line + "|"
        rospy.loginfo("[total experiment info] " + total_experiment_info_str)

        time_now = datetime.now().strftime("%m-%d-%Y--%H-%M-%S")
        image = get_image(bridge)

        cv2.imwrite("/home/mmatak/catkin_ws/src/grasp_pipeline/src/grasp_client/results/" + str(object_id) + time_now + "-lifted-" + method_used + ".jpeg", image)

        counter+=1

def save_to_disk(object_info, est_obj_pose, palm_pose, hand_config, object_id, fingertips_in_contact, hand_config_in_contact, preshape_image, in_contact_image, lifted_image, hand_ftips_poses, success, reason):
    width = object_info.width
    height = object_info.height
    depth = object_info.depth
    now = datetime.now()
    time = now.strftime("%m-%d-%Y--%H-%M-%S")
    path = "/mnt/data_collection/" + str(object_id) + "-" + time
    locations, orientations = hand_ftips_poses

    print("saving to disk...")
    fh = open(path + ".txt", "w")
    string = ""
    string += "object_id=" + str(object_id) + "\n"
    string += "true_mesh_pose(world)=" + str(object_info.pose) + "\n"
    string += "est_obj_pose(world)=" + str(est_obj_pose) + "\n"
    string += "object width=" + "{:.4f}".format(width) + "\n"
    string += "object height=" + "{:.4f}".format(height) + "\n"
    string += "object depth=" + "{:.4f}".format(depth) + "\n"
    string += "palm_pose(object frame)=" + str(palm_pose) + "\n"
    string += "hand_config_preshape=" + str(hand_config.position) + "\n"
    string += "hand_config_after_closing=" + str(hand_config_in_contact.position) + "\n"
    string += "hand_ftips_locations_after_closing(object frame)=" + str(locations) + "\n"
    string += "hand_ftips_orientations_after_closing(object frame)=" + str(orientations) + "\n"
    string += "fingertips in contact=" + str(fingertips_in_contact) + "\n"
    string += "success=" + str(success) +  "\n"
    string += "reason=" + str(reason) +  "\n"
    fh.write(string)
    fh.close()
   
    pcd = PointCloud.from_msg(object_info.cloud)
    pcd.save_pcd(path + ".pcd")

    cv2.imwrite(path + "-preshape.jpeg", preshape_image)
    cv2.imwrite(path + "-in-contact.jpeg", in_contact_image)
    if lifted_image is not None:
        cv2.imwrite(path + "-lifted.jpeg", lifted_image)

    print("saved to disk!")

def get_image(bridge):
    imgmsg = rospy.wait_for_message("/camera/rgb/image_raw", Image)
    return bridge.imgmsg_to_cv2(imgmsg, "bgr8")


if __name__ == '__main__':
    main()
