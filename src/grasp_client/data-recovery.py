import pickle
from grasp_interface import GraspClientInterface
from robot_movements import move_robot_home

def main():
    grasp_interface = GraspClientInterface(simulation = True, reconstruct_mesh = False, icp=False, hand_only = False)
    objects = {}
    move_robot_home(grasp_interface.moveit_planner, grasp_interface.hand_client, grasp_interface.scene_manager)
    while True:
        grasp_interface.scene_manager.update_simulation_scene()
        object_name = grasp_interface.scene_manager.object_name
        print("object name: ", object_name)
        if object_name in objects:
            break
        # contains pose and dimensions
        response = grasp_interface.segment_object(pose_ground_truth=False)
        object_info = {}
        object_info["pose"] = response.obj.pose
        object_info["height"] = response.obj.height
        object_info["width"] = response.obj.width
        object_info["depth"] = response.obj.depth
        objects[object_name] = object_info
        print("Total number of processed objects: " + str(len(objects)))

    # save to disk
    with open("objects_info.pkl","wb") as f:
        pickle.dump(objects, f, 0)

if __name__ == '__main__':
    main()
