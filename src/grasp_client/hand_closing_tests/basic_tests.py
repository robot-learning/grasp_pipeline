import sys
import roslib.packages as rp
import numpy as np

from test_utils import add_object, initialize_scene
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/grasp_client/") 
from grasp_interface import GraspClientInterface
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/hand_closing/") 
from hand_closing_utils import get_closest_points,  get_closest_points_via_TF, convert_to_palm_frame, convert_to_palm_frame_via_TF, fk_for_finger, get_normals_on_fingertips, get_normals_on_closest_points
sys.path.append(rp.get_pkg_dir('tf_tools') + '/scripts')
from tf_client import TFClient

MESH_PATH = "/mnt/tars_data/sim_dataset/YCB/YCB_mesh/021_bleach_cleanser/021_bleach_cleanser_proc.stl"


def main():
    grasp_interface = GraspClientInterface(simulation = False, reconstruct_mesh = False, icp=True, hand_only = True)
    hand_kdl = grasp_interface.hand_kdl
    trans_matrix, rot_matrix, obj_mesh = initialize_scene(grasp_interface)
    print("scene initialized")
    q = grasp_interface.hand_client.get_joint_state().position
    print("joint state obtained")

    ''' 
    test closest point computation 
    '''

    # get the closest points
    closest_points, distances, triangles_ids = get_closest_points(q, obj_mesh, hand_kdl, np.array(trans_matrix), rot_matrix)
   
    # visualize closest points
    visualization = TFClient()
    visualization.update_closest_points(closest_points)
    raw_input("closest points should be visualized now")
    
    '''
    test normals on the closest points 
    '''
    
    # get the normals on the closest points
    _, object_normals_tails, object_normals_tips = get_normals_on_closest_points(q, obj_mesh, hand_kdl, trans_matrix, rot_matrix, single_vector=False)

    # visualize normals on the object
    visualization.update_object_normals_markers(object_normals_tails, object_normals_tips, "palm_link")

    '''
    test normals on the fingertip surface
    '''

    # get the normals on the fingertip surfaces
    normals_fingertips_tails, normals_fingertips_tips = get_normals_on_fingertips(q, hand_kdl)

    # visualize normals on the fingertip surfaces
    visualization.update_fingertips_normals_markers(normals_fingertips_tails, normals_fingertips_tips, "palm_link")

    raw_input("all normals are visualized now")

if __name__ == '__main__':
    main()
