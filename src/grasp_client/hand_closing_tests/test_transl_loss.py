import sys
import numpy as np
import roslib.packages as rp

sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/grasp_client/") 
from grasp_interface import GraspClientInterface
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/hand_closing/") 
from loss_function import get_translational_loss, get_translational_gradient
from test_utils import initialize_scene
from hand_closing_utils import get_closest_points


def test_transl_optimization(q0, obj_mesh, hand_kdl, trans_matrix, rot_matrix):
    all_fingers = True
    it = 0
    q_flat = list(q0)
    print("q flat: ")
    loss = get_translational_loss(q_flat, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
    print("initial loss: " + str(loss))
    print("optimizing..")
    while it<15:
        it += 1
        closest_points, _, _ = get_closest_points(q_flat, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
        gradients = get_translational_gradient(q_flat, hand_kdl, np.array(closest_points))
        q_flat = update_step(q_flat, gradients, 10)
        print("loss: ", get_translational_loss(q_flat, obj_mesh, hand_kdl, trans_matrix, rot_matrix))
        raw_input("iteration finished, press enter to continue")
    loss = get_translational_loss(q_flat, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
    print("final loss: " + str(loss))
    return q_flat

def update_step(q, gradients, step_size):
    for i in range(len(q)):
        q[i] = q[i] - step_size * gradients[i]
    return q


def test_transl_loss_fn(obj_mesh, hand_kdl, trans_matrix, rot_matrix):
    '''
    This tests that the translational loss is decreased for a finger
    when the finger is moving in the right way.
    '''
    # initial setting
    q_flat = [0.0 for i in range(16)]
    q_flat[12] = 0.4

    # closest points and their normals
    loss_previous = None
    for i in range(3):
        loss = get_translational_loss(q_flat, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
        if loss_previous is None:
            loss_previous = loss[0]
        else:
            # assert loss is decreasing
            assert loss_previous > loss[0]
        q_next = list(q_flat)
        q_next[3] += 0.2 # move index finger toward the object
        q_flat = q_next[:]

def main():
    grasp_interface = GraspClientInterface(simulation = False, reconstruct_mesh = False, icp=True, hand_only = True)
    hand_kdl = grasp_interface.hand_kdl
    hand_client = grasp_interface.hand_client
    trans_matrix, rot_matrix, obj_mesh = initialize_scene(grasp_interface)

    '''
    test translational loss function is decreasing properly
    '''
    test_transl_loss_fn(obj_mesh, hand_kdl, trans_matrix, rot_matrix)
 
    '''
    test translational loss optimization
    '''
   
    q0 = hand_client.get_joint_state().position
    q_result = test_transl_optimization(q0, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
    hand_client.send_pos_cmd(q_result, False)
    print("all tests passed successfully")

if __name__ == '__main__':
    main()
