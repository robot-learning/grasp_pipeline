import sys
import numpy as np
import roslib.packages as rp
import time

sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/grasp_client/") 
from grasp_interface import GraspClientInterface
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/hand_closing/") 
from loss_function import get_rotational_loss
from loss_function_old import get_error_vectors
from hand_closing_utils import get_closest_points, forward_kinematics_kdl, backtracking_line_search, project_to_limits
from line_search import run_optimization

sys.path.append(rp.get_pkg_dir("tf_tools") + "/scripts")
from tf_client import TFClient
from hand_closing_utils import get_normals_on_closest_points, fk_for_finger,convert_to_palm_frame, convert_to_palm_frame_inverse


from contact_invariant import get_loss, get_loss_function_gradient
from test_utils import initialize_scene
from hand_closing_utils import get_closest_points
from line_search import run_optimization
sys.path.append(rp.get_pkg_dir("allegro_collision") + "/scripts/") 
from allegro_collision_client import AllegroCollisionClient

# visualization
from hand_closing_utils import get_normals_on_closest_points
# sys.path.append(rp.get_pkg_dir('tf_tools') + '/scripts')
# from tf_client import TFClient
import rospy


def stop_condition_satisfied(loss_r, loss_t):
    translation_small = False
    rotation_small = False

    close_enough = 0
    for loss in loss_t:
        if loss < 1e-3:
            close_enough += 1
    if close_enough == 4:
        print("all translational losses are < 1e-3")
        print(loss_t)
        translation_small = True

    close_enough = 0
    for loss in loss_r:
        if loss < 0.05:
            close_enough += 1
    if close_enough == 4:
        print("all rotational losses are small enough")
        print(loss_r)
        rotation_small = True

    return translation_small and rotation_small


def test_basic_optimization(q0, obj_mesh, hand_kdl, trans_matrix, rot_matrix, hand_client = None, tf_client=None):
    print("starting optimization..")

    start = time.time()
    # initial position of the virtual point
    closest_points, _, _ = get_closest_points(q0, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
    virtual_points = []
    for i in range(4):
        finger_fk = fk_for_finger(i, q0[i*4:(i+1)*4], hand_kdl)
        finger_fk = np.squeeze(np.asarray(finger_fk[0]))
        closest_point = closest_points[:, i]
        virtual_point = finger_fk + (closest_point - finger_fk) * 0.5
        virtual_points.append(virtual_point)

    state =  np.concatenate((q0,virtual_points), axis=None)
    q = q0[:]

    if tf_client is not None:
        p = state[16:]
        p_np = np.zeros((4,3))
        p_np[0,:] = p[:3]
        p_np[1,:] = p[3:6]
        p_np[2,:] = p[6:9]
        p_np[3,:] = p[9:]

    tf_client.update_points_markers(p_np, ["palm_link" for i in range(4)])
    raw_input("visualized")
 
    step_size = 1
    it = 0
    def loss_fn(state):
        return get_loss(state, obj_mesh, hand_kdl, trans_matrix, rot_matrix)

    while it < 50:
        loss = get_loss(state, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
        print("loss: ", loss)

        gradient, direction = get_loss_function_gradient(state, obj_mesh, hand_kdl, trans_matrix, rot_matrix, second_order = True, loss_time=None)
       
        step_size = backtracking_line_search(state, loss, gradient, direction, loss_fn)
        for i in range(len(state)):
            state[i] = state[i] - step_size * direction[i]
        q = state[:16]
        q = project_to_limits(q)
        for i in range(len(q)):
            state[i] = q[i]
        if tf_client is not None:
            p = state[16:]
            p_np = np.zeros((4,3))
            p_np[0,:] = p[:3]
            p_np[1,:] = p[3:6]
            p_np[2,:] = p[6:9]
            p_np[3,:] = p[9:]

            tf_client.update_points_markers(p_np, ["palm_link" for i in range(4)])
            raw_input("visualized")
        if hand_client is not None:
            hand_client.send_pos_cmd(q, contact_sensing=False, visualize=True, reset_tare = False)
        it += 1

    end = time.time()
    print("optimization finished; time needed: " + str(end-start) + " seconds.")

    return q

def test_optimization_API(q0, obj_mesh, hand_kdl, trans_matrix, rot_matrix, collision_client, hand_client = None):
    return run_optimization(q0, obj_mesh, hand_kdl, trans_matrix, rot_matrix, collision_client, hand_client)


def main():
    grasp_interface = GraspClientInterface(simulation = False, reconstruct_mesh = False, icp=True, hand_only = True)
    hand_kdl = grasp_interface.hand_kdl
    hand_client = grasp_interface.hand_client
    hand_client.kinematics_only = True
    hand_client.visualize = True
    trans_matrix, rot_matrix, obj_mesh = initialize_scene(grasp_interface)
  
    visualization = TFClient()
    q0 = hand_client.get_joint_state().position
    collision_client = AllegroCollisionClient()
    #q = test_optimization_API(list(q0), obj_mesh, hand_kdl, trans_matrix, rot_matrix, collision_client, hand_client)
    #hand_client.send_pos_cmd(q, False)
    
    # basic GD test
    q_result = test_basic_optimization(list(q0), obj_mesh, hand_kdl, trans_matrix, rot_matrix, hand_client, visualization)
    hand_client.send_pos_cmd(q_result, contact_sensing=False, visualize=True, reset_tare = False)
    

if __name__ == '__main__':
    main()
