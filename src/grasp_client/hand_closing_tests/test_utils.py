from geometry_msgs.msg import Pose
import tf
import time
import rospy
import trimesh

OBJECT_FRAME = "estimated_object_pose"
MESH_PATH = "/mnt/tars_data/sim_dataset/YCB/YCB_mesh/021_bleach_cleanser/021_bleach_cleanser_proc.stl"
#MESH_PATH = "/mnt/tars_data/sim_dataset/YCB/YCB_mesh/004_sugar_box/004_sugar_box_proc.stl"
#MESH_PATH = "/mnt/tars_data/sim_dataset/YCB/YCB_mesh/024_bowl/024_bowl.stl"
#MESH_PATH = "/mnt/tars_data/sim_dataset/YCB/YCB_mesh/051_large_clamp/051_large_clamp.stl"

def add_object(grasp_interface):
     segmented_object_pose = Pose()
     segmented_object_pose.position.x = 0.38426229715
     segmented_object_pose.position.y = 0.009138970196
     #segmented_object_pose.position.z = 0.09712144434 # -0.12...
     segmented_object_pose.position.z = -0.12
     segmented_object_pose.orientation.x = -0.00573050320283
     segmented_object_pose.orientation.y = -0.0023592643729
     segmented_object_pose.orientation.z = -0.654406547139
     segmented_object_pose.orientation.w = 0.756117495014

     grasp_interface.visualization.update_object_pose(segmented_object_pose.position,
                                                         segmented_object_pose.orientation, 
                                                         OBJECT_FRAME)

     grasp_interface.visualization.update_marker_mesh("file://" + MESH_PATH, OBJECT_FRAME)

def initialize_scene(grasp_interface):
    home = [0.1 for i in range(16)]
    home[12] = 0.4
    grasp_interface.hand_client.send_pos_cmd(home, contact_sensing=False, visualize = True, reset_tare = False)
    add_object(grasp_interface)
    time.sleep(2)
    try:
        trans, rot = grasp_interface.listener.lookupTransform("palm_link", OBJECT_FRAME, rospy.Time(0))
    except Exception as e:
	print(e)
    rot_matrix = tf.transformations.quaternion_matrix(rot)
    return trans, rot_matrix, trimesh.load(MESH_PATH)


