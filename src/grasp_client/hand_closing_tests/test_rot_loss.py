import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/grasp_client/") 
from grasp_interface import GraspClientInterface
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/hand_closing/") 
from loss_function import get_rotational_loss, get_rotational_gradient, get_rotational_loss_log_R, get_rotational_gradient_FD
from hand_closing_utils import get_normals_on_fingertips

from test_utils import initialize_scene
from basic_tests import get_normals_on_closest_points
sys.path.append(rp.get_pkg_dir('tf_tools') + '/scripts')
from tf_client import TFClient
import numpy as np
  

def test_rot_optimization(q0, hand_kdl, object_normals, visualization=None, FD_as_gradient=False):
    all_fingers = True
    it = 0
    q_flat = list(q0)
    print("initial loss: " + str(get_rotational_loss(q_flat, hand_kdl, object_normals)))
    print("optimizing..")
    while it<500:
        it += 1
        loss = get_rotational_loss_log_R(q_flat, hand_kdl, object_normals)

        if all_fingers:
            if FD_as_gradient:
                step_size = 0.01
                gradients = get_rotational_gradient_FD(q_flat, hand_kdl, object_normals)
                for i in range(16):
                    q_flat[i] = q_flat[i] - step_size * gradients[i]
        else:
            if fd:
                # FD
                q_minus = q_flat[:]
                q_plus = q_flat[:]
                eps = 1e-5
                q_plus[3] += eps
                q_minus[3] -= eps
                q_plus_loss = get_rotational_loss(q_plus, hand_kdl, object_normals)
                q_minus_loss = get_rotational_loss(q_minus, hand_kdl, object_normals)
                fd = (q_plus_loss[0] - q_minus_loss[0]) / (2 * eps)
                index_gradient = fd
                q_flat[3] = q_flat[3] - index_gradient * q_flat[3]
            else:
                gradient = np.array(get_rotational_gradient(q_flat, hand_kdl, object_normals))
                index_gradient = gradient[0,:,:].T
                q_flat[3] = q_flat[3] - index_gradient[3,0] 
   
    print("final loss: " + str(get_rotational_loss(q_flat, hand_kdl, object_normals)))
    if visualization is not None:    
        print("Visualizing finertips normals..")
        normals_fingertips_tails, normals_fingertips_tips = get_normals_on_fingertips(q_flat, hand_kdl)
        visualization.update_fingertips_normals_markers(normals_fingertips_tails, normals_fingertips_tips, "palm_link")
    return q_flat


def test_rot_loss_fn(obj_mesh, hand_kdl, trans_matrix, rot_matrix):
    '''
    This tests that the rotational loss is decreased for a finger
    when the finger is moving in the right way.
    '''
    # initial setting
    q_flat = [0.0 for i in range(16)]
    q_flat[12] = 0.4

    object_normals, _, _ = get_normals_on_closest_points(q_flat, obj_mesh, hand_kdl, trans_matrix, rot_matrix)

    # closest points and their normals
    loss_previous = None
    for i in range(3):
        loss = get_rotational_loss(q_flat, hand_kdl, object_normals)
        if loss_previous is None:
            loss_previous = loss[0]
        else:
            # assert loss is decreasing
            assert loss_previous > loss[0]
        q_next = list(q_flat)
        q_next[3] += 0.2 # move index finger toward the object
        q_flat = q_next[:]


def test_rot_loss_equivalence(obj_mesh, hand_kdl, trans_matrix, rot_matrix):
    q = [0.1 for i in range(16)]
    q[12] = 0.4

    object_normals, _, _ = get_normals_on_closest_points(q, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
    for i in range(7):
        losses_log_R = get_rotational_loss_log_R(q, hand_kdl, object_normals)
        losses_dot_product = get_rotational_loss(q, hand_kdl, object_normals)

        assert len(losses_log_R) == 4
        for i in range(len(losses_log_R)):
            difference = abs(losses_log_R[i] - losses_dot_product[i])
            assert difference < 1e-6, difference + "i: " + i 

        q = [q[i]+0.1 for i in range(len(q))]
    print("losses are equivalent")


def test_rotation(hand_client, obj_mesh, hand_kdl, trans_matrix, rot_matrix, visualization=None):
    '''
    test equivalence of the rot_loss representation: dot product & log R tilde
    '''
    test_rot_loss_equivalence(obj_mesh, hand_kdl, trans_matrix, rot_matrix)
   
    ''' 
    test rotational loss function is decreasing properly
    '''

    test_rot_loss_fn(obj_mesh, hand_kdl, trans_matrix, rot_matrix)

    '''
    test simple GD (gradient obtained via FD)
    
    '''

    q = hand_client.get_joint_state().position
    if visualization is not None:
        print("visualizing normals on object surface..")
        _, object_normals_tails, object_normals_tips = get_normals_on_closest_points(q, obj_mesh, hand_kdl, trans_matrix, rot_matrix)

        # visualize normals on the object
        visualization.update_object_normals_markers(object_normals_tails, object_normals_tips, "palm_link")

    object_normals, _, _ = get_normals_on_closest_points(q, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
    
    q_result = test_rot_optimization(q, hand_kdl, object_normals, visualization, FD_as_gradient=True)
    hand_client.send_pos_cmd(q_result, False)


def main():
    grasp_interface = GraspClientInterface(simulation = False, reconstruct_mesh = False, icp=True, hand_only = True)
    hand_kdl = grasp_interface.hand_kdl
    trans_matrix, rot_matrix, obj_mesh = initialize_scene(grasp_interface)
    visualization = TFClient()

    test_rotation(grasp_interface.hand_client, obj_mesh, hand_kdl, trans_matrix, rot_matrix, visualization)
    print("all tests passed successfully")

if __name__ == '__main__':
    main()
