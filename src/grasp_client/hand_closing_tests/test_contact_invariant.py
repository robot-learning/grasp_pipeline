import sys
import numpy as np
import roslib.packages as rp
import time
import rospy

sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/grasp_client/") 
from grasp_interface import GraspClientInterface
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/hand_closing/") 
from test_utils import initialize_scene
from contact_invariant import solve
sys.path.append(rp.get_pkg_dir("allegro_collision") + "/scripts/") 
from allegro_collision_client import AllegroCollisionClient

def main():
    grasp_interface = GraspClientInterface(simulation = False, reconstruct_mesh = False, icp=False, hand_only = True)
    hand_kdl = grasp_interface.hand_kdl
    hand_client = grasp_interface.hand_client
    hand_client.kinematics_only = True
    hand_client.visualize = True
    trans_matrix, rot_matrix, obj_mesh = initialize_scene(grasp_interface)
    raw_input("scene initialized")
  
    allegro_collision_client = AllegroCollisionClient()
    q0 = hand_client.get_joint_state().position

    # visualization only
    #tf_client = TFClient()
    #frames = ["palm_link" for i in range(4)]
    #tf_client.update_points_markers(virtual_points, frames)
    

    #projected_points, distances, _ = get_projection_on_object(virtual_points, obj_mesh, hand_kdl, trans_matrix, rot_matrix)

    #for i in range(4):
    #TODO: not working because it's only one point that can be projected
    # tf_client.update_projected_point(projected_points[i], "palm_link")
    #    continue

    # test that loss function runs
    #get_loss(state, obj_mesh, hand_kdl, trans_matrix, rot_matrix)

    # test the solver
    start = time.time()
    q = solve(q0, obj_mesh, hand_kdl, trans_matrix, rot_matrix, allegro_collision_client)
    end = time.time()
    print("time in seconds: ", end-start)

    # visualize result
    hand_client.send_pos_cmd(q, contact_sensing = False, visualize = True, reset_tare = False)

    '''
    
    # basic GD test
    iteration = 0
    loss = 1
    while loss > 1e-6:
        loss = get_loss(state, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
        state = state - get_loss_gradient(state, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
        iteration += 1
        if iteration % 1000 == 0:
            points = [state[16:], state[16:], state[16:], state[16:]]
            frames = ["palm_link" for i in range(4)]
            tf_client.update_points_markers(points, frames)
 
            points, distances, _ = get_projection_on_object(points, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
            tf_client.update_projected_point(points[0], "palm_link")

            print(state[:16])
            print(loss)
            print("iteration: ", iteration)
            raw_input("send pos")
            hand_client.send_pos_cmd(state[:16], False)

        if iteration % 100000 == 0:
            break
   ''' 

if __name__ == '__main__':
    main()
