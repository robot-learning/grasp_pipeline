import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("hand_services") + "/scripts")
from hand_client import HandClient
sys.path.append(rp.get_pkg_dir('ll4ma_kdl') + '/scripts')
from handModel import HandModel
from line_search import run_optimization, get_current_state
from hand_closing_utils import forward_kinematics_kdl
from sensor_msgs.msg import JointState
import numpy as np
import rospy
sys.path.append(rp.get_pkg_dir('tf_tools') + '/scripts')
from grasp_pipeline.srv import *
sys.path.append(rp.get_pkg_dir('grasp_pipeline') + '/src/grasp_client')
from robot_movements import move_palm


'''
This file tests the optimization procedure.
'''

ROSPARAM_ALLEGRO_DESCRIPTION = "allegro/robot_description"

class MockHandClient:
    def __init__(self, kdl, hand_client, dynamics=True):
        self.q = [0 for i in range(16)]
        self.kdl = kdl
        self.hand_client = hand_client
        self.dynamics = dynamics

    def get_joint_state(self):
        state = JointState()
        state.position = self.q
        return state

    def send_pos_cmd(self, q):
        if self.dynamics:
            print("sending command...")
            self.hand_client.send_pos_cmd(q, contact_sensing = False)
            print("command sent")
            print("waiting for states")
            self.q = self.hand_client.get_joint_state().position
            print("new joint state retrieved ")
            return
        self.q = q
        print("waiting for service..")
        rospy.wait_for_service("update_robot_state")
        proxy = rospy.ServiceProxy("update_robot_state", UpdateRobotState)
        req = UpdateRobotStateRequest()
        req.joint_state = q
        raw_input("press enter to send joints commands")
        proxy(req)

    def get_biotac_contact_readings(self):
        return [False, False, False, False]

    def get_jacobians(self):
        index = self.kdl.Jacobian(self.q[:4], 0)
        middle = self.kdl.Jacobian(self.q[4:8], 1)
        ring = self.kdl.Jacobian(self.q[8:12], 2)
        thumb = self.kdl.Jacobian(self.q[12:], 3)
        return index, middle, ring, thumb

    def reset_position(self):
        print("resetting position")
        self.q = [0.0 for i in range(16)]
        self.send_pos_cmd(self.q)
        print("hand position resetted")

def parse_test_samples(filename):
    arm_qs = []
    hand_qs = {}
    with open(filename) as fp:
        for line_nr, line in enumerate(fp):
            if "arm" in line:
                parsing_arm = True
                parsing_hand = False
            elif "hand" in line:
                parsing_hand = True
                parsing_arm = False
            else:
                print("wtf")
            chunks = line.split(":")
            q_string = chunks[1]
            q_strings = q_string.split(",")
            q = []

            for i in range(len(q_strings)):
                q.append(float(q_strings[i]))

            if parsing_hand:
                assert len(q) == 16

                # initial "go home" position
                if len(arm_qs) == 0:
                    continue

                if len(arm_qs) not in hand_qs:
                    hand_qs[len(arm_qs)] = []

                hand_qs[len(arm_qs)].append(q)

            if parsing_arm:
                assert len(q) == 7
                arm_qs.append(q)

    print("parsing of tests finished")
    return arm_qs, hand_qs



arm_qs, hand_qs = parse_test_samples("tests.txt")
#hand_kdl = HandModel(ROSPARAM_ALLEGRO_DESCRIPTION)
#hand_client = MockHandClient(hand_kdl)
hand_client = MockHandClient(None, HandClient(init_node=True))
raw_input("press enter to send 0s")
hand_client.reset_position()
raw_input("0s sent. Press enter to continue")
errors = {}
for i in range(16):
    errors[i] = []

q_desired_index = [-0.2, 0.3, 0.7, 0.6]
q_desired_middle = [-0.26, 0.4, 0.8, 0.5]
q_desired_ring = [-0.3, 0.5, 1.2, 0.4]
q_desired_thumb = [1.38, 0.3, 0.61, -0.21]
q_desired = q_desired_index + q_desired_middle + q_desired_ring + q_desired_thumb
#move_palm([0,0,0,-0.5,1.23, -0.5, 0.5])
raw_input("press enter to do one-shot experiment")
hand_client.send_pos_cmd(q_desired)
raw_input("desired position sent")

raw_input("press enter to start experiments")
for i, arm_q in enumerate(arm_qs):
    move_palm(arm_q)
    print(hand_qs[i+1])
    for q_desired in hand_qs[i+1]:
        hand_client.send_pos_cmd(q_desired)
        raw_input("desired position sent")

        for joint in range(16):
            error = q_desired[joint] - hand_client.q[joint]
            errors[joint].append(error)

print("total number of experiments: ", str(len(errors[0])))
for i in range(16):
    print("joint " + str(i) + " mean err: " + str(np.mean(errors[i])))
    print("joint " + str(i) + " std dev: " + str(np.std(errors[i])))
    print()

    #hand_client.reset_position()

raw_input("experiments finished")

goal_positions, _ = forward_kinematics_kdl(q_desired_index, q_desired_middle, q_desired_ring, q_desired_thumb, hand_kdl)

run_optimization(goal_positions, hand_client, hand_kdl)

result_index, result_middle, result_ring, result_thumb = get_current_state(hand_client)
resulting_positions, _ = forward_kinematics_kdl(result_index, result_middle, result_ring, result_thumb, hand_kdl)

for i in range(4):
    result = np.linalg.norm(resulting_positions[:, i])
    goal = np.linalg.norm(goal_positions[:, i])
    print(goal_positions[:,i])
    print(resulting_positions[:,i])
    print()

