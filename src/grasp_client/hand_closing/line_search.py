import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir('tf_tools') + '/scripts')
from tf_client import TFClient
import tf
import numpy as np
import rospy
from vis_tools import visualize_losses, visualize_joints
from contact_invariant import get_loss, get_loss_function_gradient
from hand_closing_utils import backtracking_line_search, get_closest_points, get_normals_on_closest_points, fk_for_finger, project_to_limits
import random as rand

DISTANCE_THRESHOLD = 0.001 # in meters
GRADIENT_THRESHOLD = 1e-6 # min L2 norm of a gradient
JOINTS_INDICES = slice(0, 16)
AUX_VAR_INDICES = slice(16,28)

def run_optimization(obj_mesh, hand_kdl, trans_matrix, rot_matrix, collision_client, hand_client, in_hand_classifier=None):
    '''
    @param hand_client None if optimization is done offline
    '''
    # init variables
    visualization = TFClient()
    max_iterations = 20
    iteration = 0
    step_size = 1.0
    fingers_in_contact = [False, False, False, False] # index, middle, ring, thumb
    gradient = None

    # initial state
    q0 = list(hand_client.get_joint_state().position)
    state = get_initial_state(q0, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
   
    # for step size backtracking line search
    def loss_fn(state):
        return get_loss(state, obj_mesh, hand_kdl, trans_matrix, rot_matrix)

    # main loop
    outer_loop_counter = 0
    total_iterations = 0
    while outer_loop_counter < 3:
        while not stop_condition_satisfied(fingers_in_contact, iteration, max_iterations, gradient, step_size):
           rospy.loginfo("Iteration: " + str(iteration))

           loss = get_loss(state, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
           try:
               gradient, direction = get_loss_function_gradient(state, obj_mesh, hand_kdl, trans_matrix, rot_matrix, total_iterations+1, second_order = True)
           except np.linalg.LinAlgError as e:
               break
           step_size = backtracking_line_search(state, loss, gradient, direction, loss_fn)

           # update state 
           for i in range(len(state)):
               delta = step_size * direction[i]
               if delta<0:
                   delta_sign = -1
               else:
                   delta_sign = 1
               if i < 16:
                   if abs(delta) < 0.2:
                       state[i] = state[i] - delta
                   else:
                       state[i] = state[i] - delta_sign * 0.2
               else:
                   state[i] = state[i] - 0.3 * delta
           q = state[JOINTS_INDICES]
           hand_client.visualize_joints(q)
           q = project_to_limits(q)
           hand_client.visualize_joints(q)

           frames = ["palm_link" for i in range(4)]
           pts = np.array(state[AUX_VAR_INDICES])
           pts = np.reshape(pts, (4,3))
           visualization.update_points_markers(pts, frames)
           for i in range(len(q)):
                state[i] = q[i]
           iteration += 1
           total_iterations += 1

        # self collision projection
        q = state[JOINTS_INDICES]
        _, _, q_new = collision_client.avoid_collision(q, True)
        # loss > 1000 -> no chance of grasping
        if np.linalg.norm(q_new - q) < 1e-6 or loss > 1000:
            break
       
        # if it was in collision, repeat the optimization
        q = q_new
        state = get_initial_state(q, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
        iteration = 0
        fingers_in_contact = [False, False, False, False] # index, middle, ring, thumb
        gradient = None
        hand_client.visualize_joints(q)
        outer_loop_counter += 1
    return q

def get_initial_state(q0, obj_mesh, hand_kdl, trans_matrix, rot_matrix):
    closest_points, _ = get_closest_points(q0, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
    virtual_points = []
    for i in range(4):
        finger_fk = fk_for_finger(i, q0[i*4:(i+1)*4], hand_kdl)
        finger_fk = np.squeeze(np.asarray(finger_fk[0]))
        closest_point = closest_points[:, i]
        virtual_point = finger_fk + (closest_point - finger_fk) * 0.5
        virtual_points.append(virtual_point)
    state =  np.concatenate((q0,virtual_points), axis=None)
    return state

def stop_condition_satisfied(fingers_in_contact, iteration, max_iterations, gradient, step_size):
    if iteration > 0:
        for i in range(4):
            if not fingers_in_contact[i]:
                if np.linalg.norm(gradient[i*4:(i+1)*4]) < GRADIENT_THRESHOLD:
                    rospy.loginfo("Setting finger " + str(i) + " in contact because L2 norm of gradient is small.")
                    rospy.loginfo("Gradient norm: " + str(np.linalg.norm(gradient[i*4:(i+1)*4])))
                    fingers_in_contact[i] = True

    if sum(fingers_in_contact) == len(fingers_in_contact):
        rospy.loginfo("Stopping the optimization because all the fingers are in contact.")
        return True

    if iteration == max_iterations:
        rospy.loginfo("Stopping the optimization because max # of iterations is reached.")
        return True

    if step_size < 0.2:
        rospy.loginfo("Stopping the optimization because step size is small.")
        return True

    return False


def update_contact_info(new_info, old_info):
    # once in contact, assume contact is never broken
    return np.logical_or(old_info, new_info)

