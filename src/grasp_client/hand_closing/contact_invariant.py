import sys
import numpy as np
import roslib.packages as rp
import time
import rospy

from utils import call_service
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/hand_closing/") 
from hand_closing_utils import get_closest_points, forward_kinematics_kdl
from scipy.optimize import minimize, Bounds
sys.path.append(rp.get_pkg_dir("grasp_pipeline"))
from grasp_pipeline.srv import *
sys.path.append(rp.get_pkg_dir("tf_tools"))
from tf_tools.srv import *

from sensor_msgs.msg import JointState
from hand_closing_utils import get_normals_on_closest_points, fk_for_finger,convert_to_palm_frame, convert_to_palm_frame_inverse, get_normals_on_fingertips


def project_to_limits(q):
    for i in range(12):
        if i%4 == 0:
            q[i] = max(q[i], -0.571)
            q[i] = min(q[i], 0.571)
        elif i%4 == 1:
            q[i] = max(q[i], -0.296)
            q[i] = min(q[i], 1.711)
        elif i%4 == 2:
            q[i] = max(q[i], -0.274)
            q[i] = min(q[i], 1.809)
        elif i%4 == 3:
            q[i] = max(q[i], -0.327)
            q[i] = min(q[i], 1.718)
    for i in range(12,16):
        if i == 12:
            q[i] = max(q[i], 0.363)
            q[i] = min(q[i], 1.496)
        elif i == 13:
            q[i] = max(q[i], -0.205)
            q[i] = min(q[i], 1.263)
        elif i == 14:
            q[i] = max(q[i], -0.289)
            q[i] = min(q[i], 1.744)
        elif i == 15:
            q[i] = max(q[i], -0.262)
            q[i] = min(q[i], 1.819)
    return q


def get_projection_on_finger(finger_id, q, hand_kdl):
    finger_fk = fk_for_finger(finger_id, q[finger_id*4:(finger_id+1)*4], hand_kdl)
    finger_fk = np.squeeze(np.asarray(finger_fk[0]))
    return finger_fk

def get_projection_on_object(points, object_representation, hand_kdl, trans_matrix, rot_matrix, output_shape_same_as_input = False):
    '''
    Assumes points are in palm link frame
    '''
    assert points.shape == (4,3), points
    points_of = convert_to_palm_frame_inverse(points, trans_matrix, rot_matrix)
    closest_points_of, distances = object_representation.get_closest_points_on_surface(points_of.T)
    points_converted = convert_to_palm_frame(closest_points_of, trans_matrix, rot_matrix)
    if output_shape_same_as_input:
        points_converted = points_converted.T
        assert points_converted.shape == (4,3), points_converted.shape
        return points_converted, distances
    points_pf = [points_converted[:,0], points_converted[:,1], points_converted[:,2], points_converted[:,3]]
    return points_pf, distances

def get_rotational_loss_component(q, obj_repr, hand_kdl, trans_matrix, rot_matrix, visualize=False):
    option = 2

    # option 1: object normals
    if option==1:
        object_normals, _,_ = get_normals_on_closest_points(q,obj_repr,hand_kdl,trans_matrix,rot_matrix)
        loss_rot = get_rotational_loss(q, hand_kdl, object_normals)
    elif option == 2:
    # option 2: approach vector
#        closest_points, _, _ = get_closest_points(q, obj_repr, hand_kdl, trans_matrix, rot_matrix)
        fk_pf, _ = forward_kinematics_kdl(q[:4],q[4:8],q[8:12],q[12:], hand_kdl)
        fk_pf = fk_pf.T
        closest_points_pf, _ = get_projection_on_object(fk_pf, obj_repr, hand_kdl, trans_matrix, rot_matrix, True)
        assert fk_pf.shape == closest_points_pf.shape, closest_points_pf.shape
        approach_vectors = closest_points_pf - fk_pf
        for i in range(4):
            approach_vectors[i,:] /= np.linalg.norm(approach_vectors[i,:])
        if visualize:
            visualize_vectors(fk_pf, closest_points_pf, "OBJECT")

        loss_rot = get_rotational_loss(q, hand_kdl, approach_vectors, visualize)
        #print("loss rot: ", loss_rot)
        #raw_input("vectors visualized and rot loss computed. presss enter")
    else:
    # option 3: no rotational loss
        loss_rot = [0,0,0,0]
    return loss_rot


def get_rotational_loss(q_flat, hand_kdl, vectors, visualize=False):
    n_tails, n_tips = get_normals_on_fingertips(q_flat, hand_kdl)
    if visualize:
        visualize_vectors(n_tails, n_tips, "FINGERTIP")

    normals = [n_tips[i] - n_tails[i] for i in range(len(n_tails))]
    assert len(normals) == 4
    assert len(vectors) == 4
    for i, normal in enumerate(normals):
        assert abs(np.linalg.norm(normal) - 1) < 1e-6, np.linalg.norm(normal)
        assert abs(np.linalg.norm(vectors[i]) - 1) < 1e-6, np.linalg.norm(vectors[i])
    loss = []
    for i in range(4):
        loss.append(1 - np.dot(np.array(normals[i]).T, vectors[i]))
    return loss

def visualize_vectors(tails, tips, mode):
    print("waiting for visualization service...")
    rospy.wait_for_service("visualize")
    proxy = rospy.ServiceProxy("visualize", Data)
    req = DataRequest()
    req.control_mode = 6
    req.string_data = ["palm_link", mode]

    req.normal_0_tail = tails[0]
    req.normal_1_tail = tails[1]
    req.normal_2_tail = tails[2]
    req.normal_3_tail = tails[3]

    for i in range(4):
        direction = tips[i] - tails[i]
        tips[i] = tails[i] + direction / np.linalg.norm(direction)

    req.normal_0_tip = tips[0]
    req.normal_1_tip = tips[1]
    req.normal_2_tip = tips[2]
    req.normal_3_tip = tips[3]

    proxy(req)

def get_loss(state, obj_repr, hand_kdl, trans_matrix, rot_matrix, visualization = None, loss_time=None):
    rospy.loginfo("get_loss start")

    q = state[:16]
    loss_rot = get_rotational_loss_component(q, obj_repr, hand_kdl, trans_matrix, rot_matrix, visualize=True)
    loss_transl = get_transl_loss_component(state, obj_repr, hand_kdl, trans_matrix, rot_matrix, visualization)
    #loss_in_hand = get_loss_in_hand(q)
    #print("loss transl: ", loss_transl)
    #TODO: weighting? in hand?
    return np.linalg.norm(loss_transl) + np.linalg.norm(loss_rot)# + loss_in_hand

def get_loss_in_hand(q):
    req = GraspInfoRequest()
    js = JointState()
    js.position = q
    req.full_allegro_joint_state = js

    service_name = "evaluate_in_hand"
    resp = call_service(service_name, req, GraspInfo)
    log_likelihood = resp.in_hand_success_log_likelihood
    nll = -log_likelihood
    return nll

def get_transl_loss_component(state, obj_repr,  hand_kdl, trans_matrix, rot_matrix, visualization = None):
    q = state[:16]
    virtual_points = state[16:]
    virtual_points = [virtual_points[i*3:(i+1)*3] for i in range(4)]
    
    projections_obj, distances = get_projection_on_object(np.array(virtual_points), obj_repr, hand_kdl, trans_matrix, rot_matrix)

    if visualization is not None:
        obj_np = np.zeros((4,3))
        obj_np[0] = projections_obj[0][0]
        obj_np[1] = projections_obj[1][0]
        obj_np[2] = projections_obj[2][0]
        obj_np[3] = projections_obj[3][0]
        visualization.update_projected_points(projections_obj, ["palm_link" for i in range(4)])

    loss_transl = []
    # translational loss
    for i in range(4):
        proj_obj = projections_obj[i]
        proj_hand = get_projection_on_finger(i, q, hand_kdl)
        virtual_point = virtual_points[i]
        hand_err = proj_hand - virtual_point
        obj_err  = proj_obj - virtual_point
        loss_transl.append(np.linalg.norm(100 * hand_err) ** 2 + np.linalg.norm(100 * obj_err) ** 2)

    return loss_transl

def get_virtual_point_gradient(finger_id, virtual_points, points_on_object, distances):
    point_on_object = points_on_object[finger_id]
    virtual_point = virtual_points[finger_id]
    residual_gradient = (virtual_point - point_on_object) / (np.linalg.norm(1e-6 + point_on_object - virtual_point) + 1e-10)
    sdf = distances[finger_id]
    return residual_gradient, sdf

def get_finger_component_gradient(finger_id, state, hand_kdl, virtual_points):
    q = state[finger_id*4:(finger_id+1)*4]
    finger_fk = fk_for_finger(finger_id, q, hand_kdl)
    finger_fk = np.squeeze(np.asarray(finger_fk[0]))
    direction = (finger_fk - virtual_points[finger_id])
    positional_jacobian = hand_kdl.Jacobian(q,finger_id)[:3,:]
    return positional_jacobian, direction

def get_loss_function_gradient(state, obj_repr, hand_kdl, trans_matrix, rot_matrix, iteration, second_order = False, loss_time=None):
    virtual_points = state[16:]
    virtual_points = [virtual_points[i*3:(i+1)*3] for i in range(4)]
    fingers_joints = state[:16]

    projections_obj, distances = get_projection_on_object(np.array(virtual_points), obj_repr, hand_kdl, trans_matrix, rot_matrix)

    # for gradient descent this is gradient, but
    # this can also use second order information, hence the naming 'direction'
    finger_direction = []
    finger_gradient = []
    virtual_points_direction = []
    virtual_points_gradient = []
    
    req = GraspInfoRequest()
    js = JointState()
    js.position = fingers_joints
    req.full_allegro_joint_state = js

    # in hand classifier 
    #service_name = "evaluate_in_hand"
    #resp = call_service(service_name, req, GraspInfo)

    #log_likelihood = resp.in_hand_success_log_likelihood
    #log_likelihood_gradient = resp.in_hand_gradient_q
    # important: we are minimizing negative log likelihood
    #in_hand_nll = -log_likelihood
    #in_hand_nll_gradient = -np.array(log_likelihood_gradient)

    rot_loss_all = get_rotational_loss_component(fingers_joints, obj_repr, hand_kdl, trans_matrix, rot_matrix)
    print("rot loss: ", (1.0/iteration**2) * np.array(rot_loss_all))
    for i in range(4):
        residual_gradient, sdf = get_virtual_point_gradient(i, virtual_points, projections_obj, distances)
        positional_J, direction = get_finger_component_gradient(i, state, hand_kdl, virtual_points)

        #  0,  0,  0,  0,  p0, p1, p2 -> p to object gradient 
        # q0, q1, q2, q3, 0, 0, 0 -> ftip to p gradient
        # q0, q1, q2, q3, 0, 0, 0 -> ftip to p gradient
        # q0, q1, q2, q3, 0, 0, 0 -> ftip to p gradient
        # q0, q1, q2, q3, 0, 0, 0 -> rotation loss gradient
        # q0, q1, q2, q3, 0, 0, 0 -> in hand classifier gradient

        rot_loss_finger = rot_loss_all[i]
        rot_gradient = np.zeros(4)
        for j in range(4):
            q_plus = np.array(fingers_joints)
            q_minus = np.array(fingers_joints)
            q_plus[i*4 + j] += 1e-3
            q_minus[i*4 + j] -= 1e-3
            rot_loss_plus = get_rotational_loss_component(q_plus, obj_repr, hand_kdl, trans_matrix, rot_matrix)
            rot_loss_minus = get_rotational_loss_component(q_minus, obj_repr, hand_kdl, trans_matrix, rot_matrix)
            rot_gradient[j] = (rot_loss_plus[i] - rot_loss_minus[i]) / 2e-3

        # loss function jacobian
        J = np.zeros((5,7))
        J[1:4,0:4] = positional_J
        J[1:4,4:7] = -1 * np.eye(3)
        J[0,4:7] = residual_gradient
        J[4,0:4] = rot_gradient
        #print("rot gradient: ", rot_gradient)
        #J[5,0:4] = in_hand_nll_gradient[i*4:(i+1)*4]
        #TODO use in hand nll

        # residual vector, i.e. determines gradient magnitude
        r_matrix = np.zeros((5,1))
        r_matrix[0,0] = sdf
        r_matrix[1:4,0] = direction
        r_matrix[4,0] = (1.0/iteration**2) * rot_loss_finger
        #r_matrix[4,0] = in_hand_nll
       
        #print("rot loss: ", rot_loss_finger)
        gradient = np.dot(J.T, r_matrix)[:,0]
        direction = gradient
        if second_order:
            #direction = np.dot(np.dot(np.linalg.inv(np.dot(J.T, J)), J.T), r_matrix)
            '''
            # damped pseuodinverse
            M = np.dot(J.T, J)
            M_inverse = None
            while M_inverse is None:
                try:
                    M_inverse = np.linalg.inv(M)
                    rospy.loginfo("inverse succeded")
                    rospy.loginfo(str(M))
                except Exception as e:
                    rospy.loginfo("inverse failed: ")
                    rospy.loginfo(str(e))
                    rospy.loginfo(str(M))
                    M = M + 1e-1 * np.eye(7)
            direction = np.dot(np.dot(M_inverse, J.T), r_matrix)
            '''
            direction = np.dot(np.linalg.pinv(J), r_matrix)


        finger_direction.append(direction[:4])
        finger_gradient.append(gradient[:4])

        virtual_points_direction.append(direction[4:])
        virtual_points_gradient.append(gradient[4:])

        '''
        print("loss function Jacobian: ")
        print(J)

        print("residual : ")
        print(r_matrix)

        print("gradient: ")
        print(gradient)
        print("shape: ")
        print(gradient.shape)
        '''
    finger_direction = np.array(finger_direction).ravel()
    finger_gradient = np.array(finger_gradient).ravel()

    virtual_points_direction = np.array(virtual_points_direction).ravel()
    virtual_points_gradient= np.array(virtual_points_gradient).ravel()

    direction = np.concatenate((finger_direction, virtual_points_direction))
    gradient = np.concatenate((finger_gradient, virtual_points_gradient))
    
    return gradient, direction

def get_state(q, p):
    return np.concatenate((q,p), axis=None)

def in_collision(state, allegro_collision_client, collision_time = None):
    rospy.loginfo("in collision start")

    q = state[:16]
    penetration, gradient, _ = allegro_collision_client.avoid_collision(q)

    rospy.loginfo("in collision end")
    return penetration

def in_collision_gradient(state,allegro_collision_client, gradient_time = None):
    rospy.loginfo("in collision gradient start")

    q = state[:16]
    _, gradient, _ = allegro_collision_client.avoid_collision(q)
    total_gradient = np.array(list(gradient) + [0 for i in range(len(state)-16)])

    rospy.loginfo("in collision gradient end")
    return total_gradient


def get_bounds():
    return Bounds(
         [-0.571, -0.296, -0.274, -0.327,
          -0.571, -0.296, -0.274, -0.327,
          -0.571, -0.296, -0.274, -0.327,
           0.363, -0.205, -0.289, -0.262,
           -np.inf, -np.inf, -np.inf,
           -np.inf, -np.inf, -np.inf,
           -np.inf, -np.inf, -np.inf,
           -np.inf, -np.inf, -np.inf,
         ],
         [0.571, 1.711, 1.809, 1.718,
          0.571, 1.711, 1.809, 1.718,
          0.571, 1.711, 1.809, 1.718,
          1.496, 1.263, 1.744, 1.819,
          np.inf, np.inf, np.inf,
          np.inf, np.inf, np.inf,
          np.inf, np.inf, np.inf,
          np.inf, np.inf, np.inf,
         ])

def solve(q0, obj_representation, hand_kdl, trans_matrix, rot_matrix, allegro_collision_client):
    # initial position of the virtual point
    closest_points, _, = get_closest_points(q0, obj_representation, hand_kdl, trans_matrix, rot_matrix)
    virtual_points = []
    for i in range(4):
        finger_fk = fk_for_finger(i, q0[i*4:(i+1)*4], hand_kdl)
        finger_fk = np.squeeze(np.asarray(finger_fk[0]))
        closest_point = closest_points[:, i]
        virtual_point = finger_fk + (closest_point - finger_fk) * 0.5
        virtual_points.append(virtual_point)

    state = get_state(q0, virtual_points)
    bounds = get_bounds()

    loss_time = [0, 0, 0]
    collision_time = [0]
    gradient_time = [0]

    cons = ({'type': 'ineq',
             'fun': lambda x: in_collision(x, allegro_collision_client, collision_time)})
#             'jac': lambda x: in_collision_gradient(x, allegro_collision_client, gradient_time)})
    
    rospy.loginfo("starting optimization")
    #res = minimize(get_loss, state, method="L-BFGS-B", 
    res = minimize(get_loss, state, method="SLSQP", jac=get_loss_function_gradient,
                   args=(obj_mesh, hand_kdl, trans_matrix, rot_matrix,),
                   bounds=bounds, options={'disp':True}, constraints=cons)#, 'gtol':1e-9, 'maxiter':150, 'tol':1e-6}) #constraints=cons, 
    print("collision time: ", collision_time)
    print("gradient time: ", gradient_time)
    print("loss time: ", loss_time)
    return res.x[:16]
