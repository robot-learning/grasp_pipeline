import numpy as np
import tf
import rospy
import sys
import roslib.packages as rp
from scipy.spatial.transform import Rotation as R
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/hand_closing/") 

from hand_closing_utils import normalize_vectors, compute_inverse_cholesky, fk_for_finger, get_normals_on_fingertips
import math


def get_loss(q, object_surface_normals, distances, hand_kdl):
    loss_r = get_rotational_loss(q, hand_kdl, object_surface_normals)
    loss_t = get_translational_loss(distances)
    return loss_r, loss_t

def get_loss_for_finger(finger_id, one_finger_q, object_surface_normals, distances, hand_kdl):
    #TODO: VERY INEFFICIENT: it computes loss for all fingers :)
    q = [0.0 for i in range(16)]
    q[finger_id * 4 : (finger_id + 1) * 4] = one_finger_q
    loss_r = get_rotational_loss(q, hand_kdl, object_surface_normals)
    loss_t = get_translational_loss(distances)
    return loss_r[finger_id], loss_t[finger_id]


def get_direction(q, hand_kdl, object_surface_normals, closest_points, w_rot, w_trans):
    gradient_descent = True
    gauss_newton = False
    assert not (gradient_descent and gauss_newton), "pick your method wisely :)"

    gradient_r, gradient_t = get_gradient(q, hand_kdl, object_surface_normals, closest_points)
    gradient = [w_rot * gradient_r[i] + w_trans * gradient_t[i] for i in range(len(gradient_t))]

    if gradient_descent:
        return gradient, gradient
    elif gauss_newton:
        raise Exception("not yet implemented: have to rewrite the loss function as least square problems first")
    else:
        print("not yet implemented")

def get_gradient(q, hand_kdl, obj_surface_normals, closest_points):
    gradient_r = get_rotational_gradient_FD(q, hand_kdl, obj_surface_normals, eps=1e-5)
    #gradient_r = get_rotational_gradient(q, hand_kdl, obj_surface_normals)
    gradient_t = get_translational_gradient(q, hand_kdl, closest_points)
    return gradient_r, gradient_t

def get_translational_loss(distances):
    '''
    distance between the closest point to the given configuration,
    '''
    return distances


def get_translational_gradient(q_flat, hand_kdl, closest_points):
    gradients = []
    for i in range(4):
        q = q_flat[i*4:(i+1)*4]
        ftip_position = np.array(hand_kdl.FK(q,i)[:3,3])
        closest_point = np.expand_dims(np.array(closest_points[:,i]), 1)
        v = ftip_position - closest_point # both shapes (3,1)
        J = hand_kdl.Jacobian(q,i)
        J_transl = J[:3,:]
        J_transl_t = J_transl.T
        gradient = np.dot(J_transl_t, v)
        #J_psinv = np.dot(np.linalg.inv(np.dot(J_rot_t, J_rot)), J_rot_t)
        #J_psinv = np.linalg.pinv(J_rot)
        gradients.append(gradient)
    return np.array(gradients).ravel()

def get_rotational_loss(q_flat, hand_kdl, object_surface_normals):
    n_tails, n_tips = get_normals_on_fingertips(q_flat, hand_kdl)
    normals = [n_tips[i] - n_tails[i] for i in range(len(n_tails))]
    assert len(normals) == 4
    assert len(object_surface_normals) == 4
    for normal in normals:
        assert abs(np.linalg.norm(normal) - 1) < 1e-6, np.linalg.norm(normal)
    loss = []
    for i in range(4):
        loss.append(1 - np.dot(np.array(normals[i]).T, object_surface_normals[i]))
    return loss

def get_rotational_gradient_FD(q, hand_kdl, object_surface_normals, eps=1e-5):
    gradients = []
    for i in range(16):
        q_minus = q[:]
        q_plus = q[:]
        q_minus[i] -= eps
        q_plus[i] += eps
        q_plus_loss = get_rotational_loss(q_plus, hand_kdl, object_surface_normals)
        q_minus_loss = get_rotational_loss(q_minus, hand_kdl, object_surface_normals)
        fd = (q_plus_loss[i/4] - q_minus_loss[i/4]) / (2 * eps)
        gradients.append(fd)
    return np.array(gradients).ravel()

def get_rotational_loss_log_R(q_flat, hand_kdl, object_surface_normals):
    n_tails, n_tips = get_normals_on_fingertips(q_flat, hand_kdl)
    ftips_normals = [n_tips[i] - n_tails[i] for i in range(len(n_tails))]
    loss = []
    for i in range(4):
        q = q_flat[i*4:(i+1)*4]
        omega = get_omega(q, ftips_normals[i], object_surface_normals[i])
        loss.append(1 - math.cos(np.linalg.norm(omega)))
    return loss

def get_omega(q, fingertip_normal, object_surface_normal):
    omega_hat = np.array([0,-1,0]).T
    cos_alpha = np.dot(np.array(fingertip_normal).T, object_surface_normal)
    assert abs(np.linalg.norm(fingertip_normal) - 1) < 1e-6
    assert abs(np.linalg.norm(object_surface_normal) - 1) < 1e-6
    theta = math.acos(cos_alpha)
    omega = theta * omega_hat
    return omega


def get_rotational_gradient(q_flat, hand_kdl, object_surface_normals):
    n_tails, n_tips = get_normals_on_fingertips(q_flat, hand_kdl)
    ftips_normals = [n_tips[i] - n_tails[i] for i in range(len(n_tails))]
    gradients = []
    for i in range(4):
        q = q_flat[i*4:(i+1)*4]
        omega = get_omega(q, ftips_normals[i], object_surface_normals[i])
        J = hand_kdl.Jacobian(q,i)
        J_rot = J[3:,:]
        J_rot_t = J_rot.T
        assert J_rot_t.shape == (4,3), J_rot_t.shape
        gradient = math.sin(np.linalg.norm(omega)) * np.dot(J_rot_t, omega)
        #J_psinv = np.dot(np.linalg.inv(np.dot(J_rot_t, J_rot)), J_rot_t)
        #J_psinv = np.linalg.pinv(J_rot)
        gradient = math.sin(np.linalg.norm(omega)) * np.dot(J_psinv, omega)
        gradients.append(gradient)
    return gradients
