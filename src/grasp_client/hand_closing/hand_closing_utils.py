import tf
import numpy as np
import rospy
from scipy.spatial.transform import Rotation as R

def normalize_vectors(vectors):
    v1,v2,v3,v4 = vectors
    return v1 / np.linalg.norm(v1),\
           v2 / np.linalg.norm(v2),\
           v3 / np.linalg.norm(v3),\
           v4 / np.linalg.norm(v4)

def get_normals_on_fingertips(q_flat, hand_kdl):
    '''
    returns normals in palm frame
    '''
    q = [q_flat[:4], q_flat[4:8], q_flat[8:12], q_flat[12:]]

    normals_fingertips_tails = []
    normals_fingertips_tips = []
    normal_tip_ff = np.matrix([[0.0,0.0,-1.0]]).T
    normal_base_ff = np.matrix([[0,0,0]]).T

    for i in range(4): 
        # FK
        T = fk_for_finger(i, q[i], hand_kdl, as_homogeneous = True)
        trans_ff_to_pf = T[0:3,3]
        rot_matrix_ff_to_pf = T[:3,:3]

        # vectors in palm frame
        normal_tip_pf = np.dot(rot_matrix_ff_to_pf, normal_tip_ff) + trans_ff_to_pf 
        normal_tail_pf = np.dot(rot_matrix_ff_to_pf, normal_base_ff) + trans_ff_to_pf 
        normals_fingertips_tails.append(normal_tail_pf)
        normals_fingertips_tips.append(normal_tip_pf)

    return normals_fingertips_tails, normals_fingertips_tips

'''
KDL approach
'''
def get_closest_points(q, object_representation, hand_kdl, trans_matrix, rot_matrix):
    fk_pf, _ = forward_kinematics_kdl(q[:4],q[4:8],q[8:12],q[12:], hand_kdl)
    #fk = [fk_pf[:,i] for i in range(4)]
    fk_of = convert_to_palm_frame_inverse(fk_pf.T, trans_matrix, rot_matrix) # this is 3x4 array
    closest_points_of, distances  = object_representation.get_closest_points_on_surface(fk_of.T)
    print("closest points of: ", closest_points_of.shape)
    closest_points_pf = convert_to_palm_frame(closest_points_of, trans_matrix, rot_matrix)
    # returns 3x4 array
    return closest_points_pf, distances

def convert_to_palm_frame_inverse(points, trans, rot_matrix, same_shape=False):
    trans_inv = -1 * np.dot(rot_matrix[:3,:3].T, trans)
    rot_inv = rot_matrix.T
    return convert_to_palm_frame(points, trans_inv, rot_inv, same_shape)

def convert_to_palm_frame(points, trans, rot_matrix, same_shape=False):
    '''
    The trans and rot matrix are from palm link to the frame where points are.
    Look in `convert_to_palm_frame_via_TF` for example of usage
    '''
    assert points.shape == (4,3), points
    trans = np.matrix(trans).T
    result = np.zeros((3,4)) # every point is xyz, hence 3 rows, there is 4 fingers, hence 4 columns
    for i, point_in_object_frame in enumerate(points):
        point_in_object_frame = np.matrix(np.append(point_in_object_frame, 1)).T
        point_in_palm_frame = np.dot(rot_matrix,  point_in_object_frame)[:3,0] + trans
        result[:,i] = np.asarray(point_in_palm_frame).ravel()
    if same_shape:
        return result.T
    return result

def forward_kinematics_kdl(q_index, q_middle, q_ring, q_thumb, hand_kdl):
    '''
    @param q_finger as numpy column vector 
    '''
    index_pos, index_rot = fk_for_finger(0, q_index, hand_kdl)
    middle_pos, middle_rot = fk_for_finger(1, q_middle, hand_kdl)
    ring_pos, ring_rot = fk_for_finger(2, q_ring, hand_kdl)
    thumb_pos, thumb_rot = fk_for_finger(3, q_thumb, hand_kdl)

    translation = np.zeros((3,4)) # 4 fingers (cols), x,y,z (rows)
    rotation = np.zeros((3,4)) # 4 fingers (cols), rotations (rows)

    translation[:,0] = np.squeeze(index_pos)
    rotation[:,0] = np.squeeze(index_rot)

    translation[:,1] = np.squeeze(middle_pos)
    rotation[:,1] = np.squeeze(middle_rot)
 
    translation[:,2] = np.squeeze(ring_pos)
    rotation[:,2] = np.squeeze(ring_rot)

    translation[:,3] = np.squeeze(thumb_pos)
    rotation[:,3] = np.squeeze(thumb_rot)

    return translation, rotation

def fk_for_finger(chain_id, q, hand_kdl, as_homogeneous = False, end_link = None):
    fk = hand_kdl.FK(q, chain_id, end_link) # 4x4 matrix
    if as_homogeneous:
        return fk
    position = np.squeeze(fk[0:3,3]).T 
    rotation = R.from_matrix(fk[:3,:3]).as_euler("xyz").T
    rotation = np.expand_dims(rotation, 1)
    return position, rotation # both have (3,1) shape

'''
TF approach
'''
def get_closest_points_via_TF(listener, object_frame_name, object_representation):
    fk, _ = forward_kinematics(listener, object_frame_name)
    closest_points_obj_frame, distances, triangles_ids  = object_representation.nearest.on_surface(fk.T)
    closest_points = convert_to_palm_frame_via_TF(closest_points_obj_frame, listener, object_frame_name)
    return closest_points, distances, triangles_ids

def convert_to_palm_frame_via_TF(points, listener, object_frame_name):
    # convert a point from object frame to palm link frame
    try:
        trans, rot = listener.lookupTransform("palm_link", object_frame_name, rospy.Time(0))
    except Exception as e:
        print(e)
    rot_matrix = tf.transformations.quaternion_matrix(rot)
    return convert_to_palm_frame(points, trans, rot_matrix)


def forward_kinematics(listener, frame_name):
    if frame_name == "palm_link":
        rospy.loginfo("better use method forward_kinematics_kdl(...) instead of this method!")
    # return translations, rotations of fingertips in frame_name
    translation = np.zeros((3,4)) # 4 fingers (cols), x,y,z (rows)
    rotation = np.zeros((3,4)) # 4 fingers (cols), rotations (rows)
    fingers = ["index_biotac_surface", "middle_biotac_surface", "ring_biotac_surface", "thumb_biotac_surface"] 
    try:
        for i,finger in enumerate(fingers):
            trans, rot = listener.lookupTransform(frame_name, finger, rospy.Time(0))
            translation[:,i] = trans
            rotation[:,i] = tf.transformations.euler_from_quaternion(rot) 
    except Exception as e:
        print(e)

    return translation, rotation


def compute_inverse_cholesky(A):
    '''
    Computes inverse of a square matrix using (adaptive) cholesky decomposition.
    '''
    try:
        L = np.linalg.cholesky(A)
    except np.linalg.LinAlgError:
        L = adaptive_cholesky_factorization(A)
    L_inv = np.linalg.inv(L)
    return np.dot(L_inv.T, L_inv)

def adaptive_cholesky_factorization(A, beta=1e-3):
    '''
    Nocedal and Wright Algorithm 3.3: Cholesky with Added Multiple of the Identity
    '''
    min_diag = None
    n = A.shape[0]
    for i in range(n):
        if min_diag is None or A[i,i] < min_diag:
            min_diag = A[i,i]

    if min_diag > 0:
        tau = 0
    else:
        tau = -min_diag + beta

    for i in range(100):
        try:
            L = np.linalg.cholesky(A+tau*np.identity(n))
            return L
        except np.linalg.LinAlgError:
            tau = max(2*tau,beta)

def backtracking_line_search(x, loss, loss_grad, p, loss_fn):
    '''
    Algorithm 3.1 @ Nocedal and Wright, 2006.
    '''
    alpha = 1.0
    ro = 0.8
    c = 1e-4
    while loss_fn(x + alpha * p) <= loss + c * alpha * np.dot(loss_grad.T, p):
        alpha *= ro
        if alpha < 0.05:
            break
    rospy.loginfo("alpha: " + str(alpha))
    return alpha

def get_normals_on_closest_points(q, obj_mesh, hand_kdl, trans_matrix, rot_matrix):
    closest_points, distances = get_closest_points(q, obj_mesh, hand_kdl, trans_matrix, rot_matrix)
    #TODO: this is  broken 
    raise("Exception as e")
    normals_of = []
    faces = obj_mesh.faces
    for i in range(4):
        normals_of.append(obj_mesh.face_normals[triangles_ids[i], :])
    normals_pf = convert_to_palm_frame(normals_of, trans_matrix, rot_matrix) # np.array 3x4
    normals_tails = []
    normals_tips = []
    for i in range(4):
        normals_tails.append(closest_points[:,i])
        normals_tips.append(closest_points[:,i] + normals_pf[:,i])
    
    object_normals_raw = [normals_tips[i] - normals_tails[i] for i in range(4)]
    object_normals = []
    for obj_normal in object_normals_raw:
        obj_normal = obj_normal / np.linalg.norm(obj_normal)
        object_normals.append(obj_normal)

    return object_normals, normals_tails, normals_tips
 

def project_to_limits(q):
    for i in range(12):
        if i%4 == 0:
            if q[i] < -0.571 or q[i] > 0.571:
                rospy.loginfo("q out of bounds, projecting")
            q[i] = max(q[i], -0.571)
            q[i] = min(q[i], 0.571)
        elif i%4 == 1:
            if q[i] < -0.296 or q[i] > 1.711:
                rospy.loginfo("q out of bounds, projecting")
            q[i] = max(q[i], -0.296)
            q[i] = min(q[i], 1.711)
        elif i%4 == 2:
            if q[i] < -0.274 or q[i] > 1.809:
                rospy.loginfo("q out of bounds, projecting")
            q[i] = max(q[i], -0.274)
            q[i] = min(q[i], 1.809)
        elif i%4 == 3:
            if q[i] < -0.327 or q[i] > 1.718:
                rospy.loginfo("q out of bounds, projecting")
            q[i] = max(q[i], -0.327)
            q[i] = min(q[i], 1.718)
    for i in range(12,16):
        if i == 12:
            if q[i] < -0.363 or q[i] > 1.496:
                rospy.loginfo("q out of bounds, projecting")
            q[i] = max(q[i], 0.363)
            q[i] = min(q[i], 1.496)
        elif i == 13:
            if q[i] < -0.205 or q[i] > 1.263:
                rospy.loginfo("q out of bounds, projecting")
            q[i] = max(q[i], -0.205)
            q[i] = min(q[i], 1.263)
        elif i == 14:
            if q[i] < -0.289 or q[i] > 1.744:
                rospy.loginfo("q out of bounds, projecting")
            q[i] = max(q[i], -0.289)
            q[i] = min(q[i], 1.744)
        elif i == 15:
            if q[i] < -0.262 or q[i] > 1.819:
                rospy.loginfo("q out of bounds, projecting")
            q[i] = max(q[i], -0.262)
            q[i] = min(q[i], 1.819)
    return q
