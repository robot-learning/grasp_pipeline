import numpy as np
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt

def visualize_points(points, visualization, frame_name = "palm_link"):
    #raw_input("press enter to visualize points")
    points_for_visualization = []
    for i in range(points.shape[1]):
        point = points[:,i]
        points_for_visualization.append(np.asarray(point))
    visualization.update_points(points_for_visualization, frame_name)
    print("points for visualization updated")
    raw_input("press enter to continue")

def visualize_losses(transl_losses, rot_losses, beta):
    fig, (total, transl, rot) = plt.subplots(3)
    fig.suptitle("Total loss = L_distance + " + str(beta) + "*L_rotation")

    total_losses = transl_losses + rot_losses
    total.plot([i for i in range(total_losses.shape[1])], total_losses[0,:], 'r--')
    total.plot([i for i in range(total_losses.shape[1])], total_losses[1,:], 'g--')
    total.plot([i for i in range(total_losses.shape[1])], total_losses[2,:], 'b--')
    total.plot([i for i in range(total_losses.shape[1])], total_losses[3,:], 'y--')
    total.set(ylabel="total loss")

    transl.plot([i for i in range(transl_losses.shape[1])], transl_losses[0,:], 'r--')
    transl.plot([i for i in range(transl_losses.shape[1])], transl_losses[1,:], 'g--')
    transl.plot([i for i in range(transl_losses.shape[1])], transl_losses[2,:], 'b--')
    transl.plot([i for i in range(transl_losses.shape[1])], transl_losses[3,:], 'y--')
    transl.set(ylabel="distance loss")

    rot.plot([i for i in range(rot_losses.shape[1])], rot_losses[0,:], 'r--')
    rot.plot([i for i in range(rot_losses.shape[1])], rot_losses[1,:], 'g--')
    rot.plot([i for i in range(rot_losses.shape[1])], rot_losses[2,:], 'b--')
    rot.plot([i for i in range(rot_losses.shape[1])], rot_losses[3,:], 'y--')
    rot.set(xlabel="iteration", ylabel="rotation loss")

    plt.savefig("/home/mmatak/losses")

def visualize_joints(q_index_desired, q_index_actual,
                     q_middle_desired, q_middle_actual,
                     q_ring_desired, q_ring_actual,
                     q_thumb_desired, q_thumb_actual):
    # plot 4 graphs
    # one graph per joint, all fingers on the same graph (maintain convention r,g,b,y)
    # actual value: square, desired value: triangle
    fig, joints_graphs = plt.subplots(4)
    fig.suptitle("Desired (square) and actual (triangle) joint values")
    nr_iterations = q_index_desired.shape[1]

    for i, joint_graph in enumerate(joints_graphs):
        joint_graph.plot(range(nr_iterations), q_index_desired[i,:], 'rs')
        joint_graph.plot(range(nr_iterations), q_index_actual[i,:], 'r^')
        joint_graph.plot(range(nr_iterations), q_middle_desired[i,:], 'gs')
        joint_graph.plot(range(nr_iterations), q_middle_actual[i,:], 'g^')
        joint_graph.plot(range(nr_iterations), q_ring_desired[i,:], 'bs')
        joint_graph.plot(range(nr_iterations), q_ring_actual[i,:], 'b^')
        joint_graph.plot(range(nr_iterations), q_thumb_desired[i,:], 'ys')
        joint_graph.plot(range(nr_iterations), q_thumb_actual[i,:], 'y^')
        joint_id = "q" + str(i)
        joint_graph.set(ylabel=joint_id)

    joints_graphs[-1].set(xlabel="iteration")

    plt.savefig("/home/mmatak/joints")


