from line_search import run_optimization
import numpy as np
import sys
import rospy
import tf
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir('allegro_collision') + '/scripts')
from allegro_collision_client import AllegroCollisionClient
sys.path.append(rp.get_pkg_dir('grasp_pipeline') + '/src/grasp_client/hand_closing')
from allegro_collision_client import AllegroCollisionClient
from contact_invariant import solve, visualize_vectors
from hand_closing_utils import fk_for_finger, convert_to_palm_frame, convert_to_palm_frame_inverse

CLOSING_METHODS = ["HEURISTIC", "OPTIMIZATION"]
NUM_FINGERS = 4


class HandClosingController:
    def __init__(self, hand_kdl, hand_client, tf_listener, method = "HEURISTIC", object_frame_name="true_mesh_pose"):
        assert method in CLOSING_METHODS, method
        self.hand_client = hand_client
        self.tf_listener = tf_listener
        self.method = method
        self.hand_kdl = hand_kdl
        self.collision_client = AllegroCollisionClient()
        self.object_frame_name = object_frame_name
        self.trans_matrix = None
        self.rot_matrix = None


    def update_object_representation(self, object_representation):
        self.object_representation = object_representation
        print("updated object representation")

    def close_hand(self, gauss_newton=False):
        print("closing the hand..")
        if self.method == CLOSING_METHODS[0]:
            print("using heuristic")
            self.hand_client.grasp_object(contact_sensing=True)
        elif self.method == CLOSING_METHODS[1]:
            q0 = self.hand_client.get_joint_state().position
            print("got q0")
            self.obtain_palm_object_transformation()
            print("got palm-obj transformation")
            if gauss_newton:
                q = run_optimization(
                            self.object_representation,
                            self.hand_kdl,
                            self.trans_matrix,
                            self.rot_matrix,
                            self.collision_client,
                            self.hand_client)
            #else:
            #    q = solve(list(q0), self.object_representation, self.hand_kdl, trans_matrix, rot_matrix, self.collision_client)
            self.hand_client.visualize_joints(q)
            input("joints visualized")
            self.hand_client.reset_tare()
            contact_sensing = True
            user = input("input 'y' to send pos cmd")
            if user == 'y':
                self.hand_client.send_pos_cmd(q, contact_sensing, False, True)


    def follow_sdf_gradient(self, visualize=False):
        if self.trans_matrix is None or self.rot_matrix is None:
            self.obtain_palm_object_transformation()

        q0 = list(self.hand_client.get_joint_state().position)
        if visualize:
            self.hand_client.visualize_joints(q0)

        q_cmd = []
        tips = []
        tails = []
        positions_pf = np.zeros((4,3))

        for finger_id in range(NUM_FINGERS):
            q = q0[finger_id*4:(finger_id+1)*4]
            position_pf, _ = fk_for_finger(finger_id, q, self.hand_kdl)
            positions_pf[finger_id,:] = np.ravel(position_pf)
        positions_of = convert_to_palm_frame_inverse(positions_pf, self.trans_matrix, self.rot_matrix, same_shape = True)
        gradients_of = np.zeros((4,3))
        sdfs = []

        for finger_id in range(NUM_FINGERS):
            position_of = positions_of[finger_id, :]
            sdf, gradient = self.object_representation.get_sdf_and_gradient(np.expand_dims(position_of, 0))
            sdfs.append(sdf)
            gradients_of[finger_id, :] = gradient

        gradients_pf = convert_to_palm_frame(gradients_of, self.trans_matrix, self.rot_matrix, True)

        for finger_id in range(NUM_FINGERS):
            q = q0[finger_id*4:(finger_id+1)*4]
            gradient_pf = gradients_pf[finger_id, :]
            finger_fk = positions_pf[finger_id,:]
            tip = finger_fk + gradient_pf
            tail = finger_fk
            v = tail - tip 
            #v = tip - tail
            tails.append(tail)
            tips.append(tip)
            # normalize gradient
            v /= np.linalg.norm(v)

            J_pos = self.hand_kdl.Jacobian(q, finger_id)[:3,:]
            q_next = q + np.dot(J_pos.T, v)
            q_cmd.append(np.ravel(q_next))

        q_cmd = np.ravel(np.array(q_cmd))
        # discard those in contact
        in_contact = self.hand_client.get_biotac_contact_readings()
        for finger_id in range(4):
            if in_contact[finger_id]:
                q_cmd[finger_id*4:(finger_id+1)*4] = q0[finger_id*4:(finger_id+1)*4]

        # tips and tails are in palm frame
        if visualize:
            visualize_vectors(tails, tips, "FINGERTIP")
            raw_input("vectors visualized")
            self.hand_client.visualize_joints(q_cmd)
        #raw_input("cmd q visualized")
        contact_sensing = True

        self.hand_client.send_pos_cmd(q_cmd, contact_sensing, False, True)


    def increase_stiffness_via_J(self):
        q0 = list(self.hand_client.get_joint_state().position)
        self.hand_client.visualize_joints(q0)
        # fingertip frame
        points = np.zeros((4,3))
        points[0,2]  = -1.5
        rot_matrix = np.eye(4)
        tails = []
        tips = []
        vs = [] # directions to move toward
        q_cmd = []
        for finger_id in range(NUM_FINGERS):
            q = q0[finger_id*4:(finger_id+1)*4]
            # palm frame
            T = fk_for_finger(finger_id, q, self.hand_kdl, as_homogeneous = True)
            rot_matrix[:3,:3] = T[:3,:3]
            directions = convert_to_palm_frame(points, np.ravel(T[:3,3]), rot_matrix)
            direction = directions[:,0]
            finger_fk = np.squeeze(np.asarray(T[:3,3]))
            tip = direction
            tail = finger_fk
            v = tip - tail
            tails.append(tail)
            tips.append(tip)

            J_pos = self.hand_kdl.Jacobian(q, finger_id)[:3,:]
            q_next = q + np.dot(J_pos.T, v)
            #q_next = q + np.dot(np.linalg.pinv(J_pos), v)
            q_cmd.append(np.ravel(q_next))
        q_cmd = np.ravel(np.array(q_cmd))
        # tips and tails are in palm frame
        #visualize_vectors(tails, tips, "FINGERTIP")
        #self.hand_client.visualize_joints(q_cmd)
        # v has to be in palm frame
        contact_sensing = False
        self.hand_client.send_pos_cmd(q_cmd, contact_sensing, False, True)

    def obtain_palm_object_transformation(self):
        rospy.loginfo("Computing palm link to object frame transformation..")
        try:
            trans_matrix, rot = self.tf_listener.lookupTransform("palm_link", self.object_frame_name, rospy.Time(0))
            rospy.loginfo("Transformation computed.")
            self.trans_matrix = trans_matrix
            self.rot_matrix = tf.transformations.quaternion_matrix(rot)
        except Exception as e:
            print(e)



