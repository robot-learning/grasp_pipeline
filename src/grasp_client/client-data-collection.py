#!/usr/bin/env python
import os
import rospy
import time
import trimesh
import tf
import numpy as np
import roslib
import sys
from robot_movements import move_palm_to_pose, move_robot_home, move_arm_up
from grasp_interface import GraspClientInterface
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Image
from pypcd import PointCloud
from cv_bridge import CvBridge
import cv2
from datetime import datetime

def main():
    grasp_interface = GraspClientInterface(simulation = True, reconstruct_mesh = False)
    bridge = CvBridge()
 
    while True:
        move_robot_home(grasp_interface.moveit_planner,grasp_interface.hand_client, grasp_interface.scene_manager)
        grasp_interface.scene_manager.update_simulation_scene()

        object_info, object_id = grasp_interface.move_to_preshape()
        if object_id is None:
            continue

        palm_pose = grasp_interface.get_current_palm_pose_in_object_frame()
        hand_config = grasp_interface.hand_client.get_joint_state()
        preshape_image = get_image(bridge)
        transl, _ = grasp_interface.get_true_object_pose()
        initial_z = transl[2]

        fingertips_in_contact = grasp_interface.close_hand(gauss_newton=True)

        hand_config_in_contact = grasp_interface.hand_client.get_joint_state()
        hand_ftips_poses = grasp_interface.get_ftips_poses("estimated_object_pose")
        in_contact_image = get_image(bridge)
        reason = ""
        success = False
        lifted_image = None
        est_obj_pose = grasp_interface.get_estimated_object_pose()
        '''
        if sum(fingertips_in_contact) < 2:
            reason = "only one fingertip in contact"
            save_to_disk(object_info, est_obj_pose, palm_pose, hand_config, object_id, fingertips_in_contact, hand_config_in_contact, preshape_image, in_contact_image, lifted_image, hand_ftips_poses, success, reason)
            continue
        '''
        try:
            move_arm_up(grasp_interface.moveit_planner, grasp_interface.scene_manager)
        except Exception as e:
            print(e)
            reason="moving the arm up failed"
            save_to_disk(object_info, est_obj_pose, palm_pose, hand_config, object_id, fingertips_in_contact, hand_config_in_contact, preshape_image, in_contact_image, lifted_image, hand_ftips_poses, success, reason)
            continue
        transl, _ = grasp_interface.get_true_object_pose()
        final_z = transl[2]
        success = final_z > initial_z + 0.05
        if success is False:
            reason = "object not high enough"
        lifted_image = get_image(bridge)
        save_to_disk(object_info, est_obj_pose, palm_pose, hand_config, object_id, fingertips_in_contact, hand_config_in_contact, preshape_image, in_contact_image, lifted_image, hand_ftips_poses, success, reason)

def save_to_disk(object_info, est_obj_pose, palm_pose, hand_config, object_id, fingertips_in_contact, hand_config_in_contact, preshape_image, in_contact_image, lifted_image, hand_ftips_poses, success, reason):
    width = object_info.width
    height = object_info.height
    depth = object_info.depth
    now = datetime.now()
    time = now.strftime("%m-%d-%Y--%H-%M-%S")
    path = "/mnt/data_collection/" + str(object_id) + "-" + time
    locations, orientations = hand_ftips_poses

    print("saving to disk...")
    fh = open(path + ".txt", "w")
    string = ""
    string += "object_id=" + str(object_id) + "\n"
    string += "true_mesh_pose(world)=" + str(object_info.pose) + "\n"
    string += "est_obj_pose(world)=" + str(est_obj_pose) + "\n"
    string += "object width=" + "{:.4f}".format(width) + "\n"
    string += "object height=" + "{:.4f}".format(height) + "\n"
    string += "object depth=" + "{:.4f}".format(depth) + "\n"
    string += "palm_pose(est object frame)=" + str(palm_pose) + "\n"
    string += "hand_config_preshape=" + str(hand_config.position) + "\n"
    string += "hand_config_after_closing=" + str(hand_config_in_contact.position) + "\n"
    string += "hand_ftips_locations_after_closing(object frame)=" + str(locations) + "\n"
    string += "hand_ftips_orientations_after_closing(object frame)=" + str(orientations) + "\n"
    string += "fingertips in contact=" + str(fingertips_in_contact) + "\n"
    string += "success=" + str(success) +  "\n"
    string += "reason=" + str(reason) +  "\n"
    fh.write(string)
    fh.close()
   
    pcd = PointCloud.from_msg(object_info.cloud)
    pcd.save_pcd(path + ".pcd")

    cv2.imwrite(path + "-preshape.jpeg", preshape_image)
    cv2.imwrite(path + "-in-contact.jpeg", in_contact_image)
    if lifted_image is not None:
        cv2.imwrite(path + "-lifted.jpeg", lifted_image)

    print("saved to disk!")

def get_image(bridge):
    imgmsg = rospy.wait_for_message("/camera/rgb/image_raw", Image)
    return bridge.imgmsg_to_cv2(imgmsg, "bgr8")


if __name__ == '__main__':
    main()
