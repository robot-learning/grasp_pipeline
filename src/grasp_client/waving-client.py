#!/usr/bin/env python
import os
import rospy
import time
import trimesh
import tf
import numpy as np
import roslib
import sys
from robot_movements import move_robot_home, move_arm_to_config
from grasp_interface import GraspClientInterface
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Image

def main():
    grasp_interface = GraspClientInterface(simulation = True, reconstruct_mesh = False)

    pose = Pose()
    pose.position.x=0
    pose.position.y=-0.8
    pose.position.z=0.7
    pose.orientation.x = 0
    pose.orientation.y = 0
    pose.orientation.z = 0
    pose.orientation.w = 1

    while True:
        print("moving robot home")
        move_robot_home(grasp_interface.moveit_planner, grasp_interface.hand_client, grasp_interface.scene_manager)

        print("moving to a configuration ")
        move_arm_to_config([0.51 for i in range(7)],grasp_interface.moveit_planner)
        '''

        grasp_interface.move_robot_to_object()

        grasp_interface.close_hand()

        try:
            move_arm_up()
        except Exception as e:
            reason="moving the arm up failed"
            continue
        '''

if __name__ == '__main__':
    main()
