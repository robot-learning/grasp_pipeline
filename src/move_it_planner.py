#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Pose, PoseStamped, Quaternion
from sensor_msgs.msg import JointState

import roslib.packages as rp
import sys
sys.path.append(rp.get_pkg_dir("moveit_interface") + "/src/moveit_interface/")
from util import get_plan, update_environment
from moveit_interface.srv import GetPlanResponse


class MoveItPlanner:
    def __init__(self):
        rospy.loginfo("Initializing MoveItPlanner")
        self.arm_joints = ["lbr4_j0", "lbr4_j1", "lbr4_j2", "lbr4_j3", "lbr4_j4", "lbr4_j5", "lbr4_j6"]
        self.hand_joints = ["index_joint_0", "index_joint_1", "index_joint_2", "index_joint_3",
                       "middle_joint_0", "middle_joint_1", "middle_joint_2", "middle_joint_3",
                       "ring_joint_0", "ring_joint_1", "ring_joint_2", "ring_joint_3",
                       "thumb_joint_0", "thumb_joint_1", "thumb_joint_2", "thumb_joint_3"]
        self.joint_names = self.arm_joints + self.hand_joints 
        rospy.loginfo("MoveItPlanner successfully initialized!")


    def get_plan_to_config(self, target_config, q_start=None, environment = {}, tolerance = 0.0, ignore_hand=False):
        if q_start is None:
            arm_js = rospy.wait_for_message("/lbr4/joint_states", JointState)
            if not ignore_hand:
                hand_js = rospy.wait_for_message("/allegro/joint_states", JointState)
                q_start = arm_js.position + hand_js.position
                assert len(q_start) == 16 + 7
            else:
                q_start = arm_js.position
                assert len(q_start) == 7

        joint_state = JointState()
        if len(q_start) == 23:
            joint_state.name = self.joint_names
        else:
            joint_state.name = self.arm_joints
        joint_state.position = q_start
        
        resp = get_plan(None, target_config, joint_state, "palm_link", environment,
                max_vel_factor=0.5, max_acc_factor=0.1)
        if resp is not None and resp.success:
            rospy.loginfo("Plan to the goal pose found.")
        else:
            rospy.logerr("Plan to the goal pose not found.")
        return resp.trajectory.joint_trajectory


    def get_plan_to_pose(self, palm_pose, q_start=None, environment = {}, cartesian_path=False):
        if q_start is None:
            arm_js = rospy.wait_for_message("/lbr4/joint_states", JointState)
            hand_js = rospy.wait_for_message("/allegro/joint_states", JointState)
            q_start = arm_js.position + hand_js.position
        assert len(q_start) == 16 + 7

        pose_stamped = PoseStamped()
        pose_stamped.header.frame_id = "world"
        pose_stamped.pose = palm_pose 

        joint_state = JointState()
        joint_state.name = self.joint_names
        joint_state.position = q_start

        resp = get_plan(pose_stamped, None, joint_state, "palm_link", environment,
                max_vel_factor=0.01, max_acc_factor=0.005, cartesian_path=cartesian_path)
        if resp is not None and resp.success:
            rospy.loginfo("Plan to the goal pose found.")
        else:
            rospy.logerr("Plan to the goal pose not found.")
        return resp.trajectory.joint_trajectory

    def update_env_scene(self, environment):
        retVal = update_environment(environment)
        arm_js = rospy.wait_for_message("/lbr4/joint_states", JointState)
        # hack to get visualization of env scene triggered
        self.get_plan_to_config(arm_js.position)
        return retVal

'''
    def go_to_configuration(self, q):
        self.group.set_joint_value_target(np.array(q))
        plan_goal = self.group.plan()
        return plan_goal

    def go_goal_trac_ik(self, pose):
        ik_js = self.get_ik(pose)
        return self.go_to_configuration(ik_js)

    def get_ik(self, pose): 
        self.group.clear_pose_targets()
        ik_js = self.ik_solver.get_ik(self.seed_state, pose.position.x, pose.position.y, pose.position.z,
                                        pose.orientation.x, pose.orientation.y, pose.orientation.z, 
                                        pose.orientation.w)
        if ik_js is None:
            rospy.logerr('No IK solution for motion planning!')
        return ik_js
       
    def handle_pose_goal_planner(self, req):
        plan = None
        if req.go_home:
            #plan = self.go_home()
            plan = self.go_zero()
        elif req.go_zero:
            plan = self.go_zero()
        elif req.go_to_configuration:
            plan = self.go_to_configuration(req.arm_configuration)
        elif req.ik_only:
            ik_js = self.get_ik(req.palm_goal_pose_world)
            response = PalmGoalPoseWorldResponse()
            response.arm_q_ik = ik_js
            return response
        else:
            plan = self.go_goal_trac_ik(req.palm_goal_pose_world)
            if plan is None:
                plan = self.go_goal(req.palm_goal_pose_world)
        response = PalmGoalPoseWorldResponse()
        response.success = False
        if plan is None:
            return response
        if len(plan.joint_trajectory.points) > 0:
            response.success = True
            response.plan_traj = plan.joint_trajectory
        return response

    def create_moveit_planner_server(self):
        rospy.Service('moveit_cartesian_pose_planner', PalmGoalPoseWorld, self.handle_pose_goal_planner)
        rospy.loginfo('Service moveit_cartesian_pose_planner:')
        rospy.loginfo('Reference frame: %s' %self.group.get_planning_frame())
        rospy.loginfo('End-effector frame: %s' %self.group.get_end_effector_link())
        rospy.loginfo('Robot Groups: %s' %self.robot.get_group_names())
        rospy.loginfo('Ready to start to plan for given palm goal poses.')

    def handle_arm_movement(self, req):
        self.group.go(wait=True)
        response = MoveArmResponse()
        response.success = True
        return response

    def create_arm_movement_server(self):
        rospy.Service('arm_movement', MoveArm, self.handle_arm_movement)
        rospy.loginfo('Service moveit_cartesian_pose_planner:')
        rospy.loginfo('Ready to start to execute movement plan on robot arm.')

if __name__ == '__main__':
    planner = CartesianPoseMoveitPlanner()
    #planner.create_moveit_planner_server()
    traj = planner.go_goal()
    print(traj)
    rospy.spin()
'''
