Planning
==================

For planning trajectories, this repo relies on `ll4ma_moveit` repo. The trajectory computed there should/must be smoothed out [e.g. by using MoveIt time-optimal planner](https://bitbucket.org/robot-learning/grasp_pipeline/issues/18/remove-external-smoothing-node-and-instead) since this repo only commands trajectories. Please make sure you don't send wild trajectories to the real robot.

Visualization
=================
Some parts of the pipeline are visualized using [this code](https://github.com/martinmatak/tf_tools)
