#!/usr/bin/env python
import os
import rospy
import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("grasp_pipeline") + "/src/") 
from move_it_planner import MoveItPlanner
from robot_movements import move_arm

def main():
    rospy.init_node("node")
    moveit_planner = MoveItPlanner()


    # move to preshape
    start_q = [0.0 for i in range(7)]
    goal_q = [-0.35292823467309553, 1.1114765044175599, 0.13407528126028723, -1.2284591380336902, -1.312729753207842, -1.0967276809162865, 0.7461297931387216]

    traj = moveit_planner.get_plan_to_config(start_q)#, start_q, {}
    move_arm(trajectory=traj.trajectory.joint_trajectory)
    traj = moveit_planner.get_plan_to_config(goal_q)#, start_q, {}
    move_arm(trajectory=traj.trajectory.joint_trajectory)


   
if __name__ == '__main__':
    main()
