# Contains functions for receiving joint topics and sending joint angles
import rospy
from sensor_msgs.msg import JointState

import sys
import roslib.packages as rp
sys.path.append(rp.get_pkg_dir("ll4ma_util") + "/src/ll4ma_util/")
import ros_util
import numpy as np
import time

class robotTrajInterface:
    def __init__(self,arm_prefix='/lbr4',hand_prefix='/allegro',init_node=True,traj_topic='/grasp/plan'):
        if(init_node):
            rospy.init_node('traj_interface')
        # subscribers:
        self.arm_joint_state=JointState()
        self.arm_joint_sub=None
        self.arm_joint_sub=rospy.Subscriber(arm_prefix+'/joint_states',
                                        JointState,self.arm_joint_state_cb,self.arm_joint_sub)
        self.got_state=False
        self.got_hand_state=False
        self.hand_joint_state=JointState()
        self.hand_joint_sub=None
        self.hand_joint_sub=rospy.Subscriber(hand_prefix+'/joint_states',
                                        JointState,self.hand_joint_state_cb,self.hand_joint_sub)

        #self.traj_server=trajectoryServer(100,robot_name='lbr4',topic_name=traj_topic,init_node=False)


        self.pub=rospy.Publisher(arm_prefix+'/joint_cmd',JointState,queue_size=1)
        self.loop_rate=rospy.Rate(100)


    def arm_joint_state_cb(self,joint_state):
        self.arm_joint_state=joint_state
        self.got_state=True

    def hand_joint_state_cb(self,joint_state):
        self.hand_joint_state=joint_state
        self.got_hand_state=True

    #def viz_traj(self,j_traj):
    #    self.traj_server.viz_joint_traj(j_traj)
        
    def send_jtraj(self,nominal_j_traj):
        dt = self.loop_rate.sleep_dur.to_sec()
        if len(nominal_j_traj.points) <= 1:
            j_traj = nominal_j_traj
        else:
            j_traj = ros_util.interpolate_joint_trajectory(nominal_j_traj, dt)
        while self.got_state is False:
            self.loop_rate.sleep()

        if len(j_traj.points) == 0:
            rospy.loginfo("Trajectory empty, not executing")
            return

        # before sending the joint trajectory, smoothly transfer to the initial waypoint:
        for i in range(len(j_traj.points)):
            new_jc=JointState()
            new_jc.name=j_traj.joint_names
            new_jc.position=j_traj.points[i].positions
            new_jc.velocity=j_traj.points[i].velocities
            new_jc.effort=j_traj.points[i].accelerations
            self.pub.publish(new_jc)
            self.loop_rate.sleep()

        des_js=j_traj.points[-1].positions
        reached=False
        
        start_time = time.time()
        # Check if goal is reached:
        while(reached==False):
            if time.time() - start_time >= 2:
                break
            self.loop_rate.sleep()
            err=np.linalg.norm(np.array(des_js)-np.array(self.arm_joint_state.position))
            if(err<0.01):
                reached=True
        rospy.loginfo('***Arm reached: %s' %str(reached))
        rospy.loginfo('***Arm reach error: %s' %str(err))
